## Simples assim:

```
>>> br R -i
Criando Resumo....... 100% ●●●●●●●●●●●●●●●●●●●●
==========================================================================
Lobras, Resumo dos prêmios principais                  
==========================================================================
Modalidade    Concurso Ganhadores         Pago/Acumulado   Próximo Sorteio
--------------------------------------------------------------------------
megasena        2079       0            R$ 14.977.664,94    Qui 20/09/2018
lotomania       1902       1            R$ 11.484.774,85    Sex 21/09/2018
timemania       1233       0             R$ 7.850.601,78    Qui 20/09/2018
duplasena       1841       0             R$ 6.135.191,25    Qui 20/09/2018
lotofacil       1713       3               R$ 796.502,25    Sex 21/09/2018
quina           4780       1               R$ 647.397,85    Qui 20/09/2018
diadesorte       52        1               R$ 485.937,23    Qui 20/09/2018
federal        05320     08639             R$ 350.000,00    Sáb 22/09/2018
loteca          818        49              R$ 299.340,12    Ter 25/09/2018
lotogol         1014       0                R$ 46.091,14    Qui 20/09/2018
--------------------------------------------------------------------------
```

Estas orientações cobrem a maior parte do projeto e esclarecem muitas dúvidas, foram adicionados alguns arquivos e imagens esclarecedores na pasta exemplos. Mesmo assim, restando dúvidas, mande-me uma mensagem. Leia mais na [documentação on-line](https://lobras.readthedocs.io/pt/latest/index.html).

## Motivação

Conheci a linguagem há pouco mais de cinco meses e desde então passei a amar sua simplicidade e objetividade, [graças Guido](https://pt.wikipedia.org/wiki/Guido_van_Rossum), assim tenho aprofundado os estudos na medida do possível. Com preguiça de conferir regularmente, eu sempre ficava com meus bilhetes esquecidos no fundo de alguma gaveta. Tudo o que encontrei sobre conferência automática de loterias brasileiras não me atendia completamente. Aí resolvi agir.

Agora isso é passado, nada melhor do que aprender fazendo algo útil.

## O que é isso

Um utilitário de brinquedo, testado no `MS Windows`, para conferir com certa rapidez e exatidão qualquer volume razoável de apostas lotéricas.

## Como isso trabalha

O usuário deve informar os dados preliminarmente, no `comando result` com os parâmetros das suas apostas para conferência, a entrada pode ser manual ou em arquivos no formato `csv`. Após um crítica interna o sistema consulta o concurso informado, no cache local ou na internet e faz a comparação das dezenas apostadas e sorteadas, no caso das loterias esportivas a comparação é por resultado do jogo ou da coluna marcada. Em seguida imprime um relatório na tela e, opcionalmente, salva e exibe um arquivo `html`.

Quando a informação do concurso está ausente `-c`, o sistema sempre buscará o último concurso na internet. Nesse caso, a conferência visual do número do concurso correto será responsabilidade do usuário.

Para o `comando simula` nunca haverá consultas externas.

:bulb: Se você gosta de gerenciar diariamente, use o `comando Resumo` com a diretiva `-i` apenas na primeira vez. Após isso, os últimos concursos de cada modalidade estarão disponíveis no cache local.

## Instalação

O sistema não é um utilitário instalável, portanto basta escolher um pasta de sua preferência e descarregar nela o conteúdo do pacote zipado. São vários arquivos, todos imprescindíveis, congelados com o utilitário <https://www.pyinstaller.org/>, conserve-os como estão. Agora traga sua planilha de apostas para o subdiretório `dados`, este é o seu arquivo mais importante para o qual recomendo o maior cuidado possível, pois na verdade **trata-se do seu precioso banco de dados**. Neste subdiretório já existe uma sugestão de modelo, adapte-a às suas necessidades.

Após isso, basta rodar o arquivo `br.exe` num *prompt de comando*.

Não existem outras dependências

## Exemplos

Exemplos detalhados na [documentação](https://lobras.readthedocs.io/pt/latest/comofazer.html).

Outros arquivos de exemplos encontram-se na pasta `exemplos`. Quando estiver no *prompt de comando* consulte sempre o help `-h`.

## Construído com

[Python](http://python.org/)

## Testes

O sistema foi testado muitas vezes durante um tempo razoável, mas nada garante a sua completa exatidão. Os procedimentos (todos ou individualmente) encontram-se na pasta `testes`, para executá-los você precisará clonar o repositório e ter o Python instalado. Brevemente outros testes serão sisponibilizados.

## Contribuindo

Para contribuir, por favor, entre em contato e observe que apreciamos o [Pacto do Colaborador](https://www.contributor-covenant.org/pt-br/version/1/4/code-of-conduct), para normas de conduta, amabilidade e respeito ao próximo.

## Histórico

`versão (0.1.0)`, lançamento.

## Autor

-   [@brjomma](http://telegram.me/brjomma)
-   e-mail: brjomma@gmail.com
    [mais detalhes](https://lobras.readthedocs.io/pt/latest/sobre.html)

## Licença MIT

[Veja detalhes](LICENSE).

## Agradecimentos

Sem a ajuda deles, seria impossível disponibilizar esse sistema e sua documentação. Por isso deixo meus sinceros agradecimentos.

-   [Python](http://python.org/) e suas inseparáveis baterias.
-   [lxml](https://lxml.de/) Uma biblioteca veloz e com muitos recursos fáceis de usar para processar e analisar documentos `html/xml`.
-   [Requests](http://docs.python-requests.org/) Uma biblioteca de solicitações `HTTP` para humanos.
-   [colorama](https://pypi.org/project/colorama/) Uma biblioteca que permite colorizar a tela do computador.
-   [PyInstaller](https://www.pyinstaller.org/) Programa que congela programas (pacotes) Python em executáveis ​​autônomos.
-   [Sphinx](http://www.sphinx-doc.org/pt_BR/stable/index.html#) esta maravilhosa ferramenta que torna fácil criar documentação em texto simples de maneira inteligente, baseado em [ReStructuredText](http://docutils.sourceforge.net/docs/user/rst/quickstart.html).

