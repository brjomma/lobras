﻿Lobras loterias brasileiras
============================

É um fato notório que todo brasileiro gosta de jogar. Mas não é tão óbvio que goste de conferir seus jogos. Prova disso são os eventuais prêmios de loteria milionários que caducam e são devolvidos ao governo (que pena).

A assiduidade da conferência é tão importante como o ato de jogar. Na minha opinião `"só ganha quem confere"`.

Conferir manualmente uma ou duas apostas por semana é razoável e mesmo assim sujeito a erros que, às vezes, podem ser traumáticos. Agora imagine conferir manualmente 20 apostas da lotomania, são 1000 dezenas, já 50 apostas mínimas da megasena necessitam a conferência de 300 dezenas, e por aí vai.

**Essa é a nossa proposta**. Viabilizar a conferência correta e quase instantânea para quaisquer quantidades de apostas, eliminando a canseira e liberando o seu precioso cérebro para a formulação de apostas vencedoras.

Boa sorte!
