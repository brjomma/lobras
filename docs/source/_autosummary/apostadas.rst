apostadas
=========

.. automodule:: apostadas

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      Apostada
      Dezenas
      Diadesorte
      Duplasena
      Federal
      Loteca
      Lotofacil
      Lotogol
      Lotomania
      Megasena
      Quina
      Timemania
   
   

   
   
   