﻿Agradecimentos
-------------------

Sem a ajuda deles, seria impossível disponibilizar esse sistema e sua documentação. Por isso deixo meus sinceros agradecimentos.

- `Python <http://python.org/>`_ e suas inseparáveis baterias.
- `lxml <https://lxml.de/>`_ Uma biblioteca veloz e com muitos recursos fáceis de usar para processar e analisar ``html/xml``.
- `Requests <http://docs.python-requests.org/>`_ Uma biblioteca ``HTTP`` para `Python <http://python.org/>`_, segura para consumo humano.
- `colorama <https://pypi.org/project/colorama/>`_ Uma biblioteca que permite colorizar a tela do computador.
- `PyInstaller <https://www.pyinstaller.org/>`_ Programa que congela programas Python (pacotes) em executáveis ​​autônomos.
- `Sphinx <http://www.sphinx-doc.org/pt_BR/stable/index.html#>`_ esta maravilhosa ferramenta que torna fácil criar documentação em texto simples de maneira inteligente, baseado em `ReStructuredText <http://docutils.sourceforge.net/docs/user/rst/quickstart.html>`_.

