Apresentação
=====================

Este sistema não adota banco de dados em sua forma tradicional, mas tem suporte a um cache local e transitório através do módulo :mod:`shelve`, que tem o simples objetivo de evitar consultas repetitivas ao site da Caixa. 

Normalmente, uma determinada consulta ao site é realizada apenas uma vez. Depois, mantido o mesmo concurso, o resultado será obtido no cache, até que haja uma nova consulta forçada através da opção ``-i`` ou o arquivo ``rcache.dat`` seja apagado pelo usuário através da opção ``-X``, ou deletando-o no disco.

O foco aqui é a conferência de resultados e não estatística ou análise dos jogos. Como bônus você poderá simular apostas livremente, determinando a quantidade de apostas, o número de dezenas e também as dezenas fixas (da sorte ou do coração).

.. contents:: continue lendo...
    :depth: 2
    :local:

Premissas
---------------

Tenha força de espírito e bom humor para entender que tudo aqui não passa de divertimento e nada deve ser encarado com a seriedade e o brilhantismo dos profissionais.
    - Considere este sistema apenas um brinquedo.
    - Todo concurso, segundo as regras da Caixa, tem validade de 90 dias. Este sistema não se interessa em armazenar os dados históricos e estatísticos. Na Internet existem incontáveis sites com `webserver <https://developer.mozilla.org/pt-BR/docs/Learn/Common_questions/o_que_e_um_web_server>`_, dispostos a explorar o assunto comercialmente. É uma alternativa.
    - Todo o sistema depende da disponibilização dos :doc:`dadosdacaixa` com o qual não se tem nenhum vínculo. Assim podem haver interrupções abruptas e sem aviso.


..  _vencidos:

O que você pode esperar
------------------------------

    .. warning::
        A princípio, o sistema não confere apostas de um concurso já vencido. Uma simples razão para isso é a grande frustração em descobrir que você ganhou mas não pode mais levar. Isso basta? Se não, você ainda poderá conferir o dito cujo acrescentando a opção ``-V``. Mas isso é puro masoquismo.

Tentamos fazer as conferências com velocidade e consistência, dentro da maior simplicidade possível.

    - **Conferência rápida de suas apostas**  Este sistema confere com certa rapidez e exatidão qualquer volume razoável de apostas.
    - **Alerta ao ganhador**  Toda vez que você ganhar algum prêmio será alertado instantaneamente. 

O que você não deve esperar
------------------------------

Existem centenas ou milhares de sites bem elaborados sobre jogos e loterias. Aqui, **não** espere nada semelhante a:
    - Análises e estatísticas elaboradas.
    - Conselhos e sugestões sobre jogos e maneiras de apostar.
    
Testes de consistência
------------------------------
Foram feitos alguns testes básicos no código. Tantos quanto a memória ajuda, mas o assunto está longe de ser esgotado, portanto, mais uma vez: desculpe e tenha paciência e a cordialidade de alertar sobre as falhas encontradas. Os procedimentos (todos ou individualmente) encontram-se na pasta `testes`, para excutá-los você precisará clonar o repositório e ter o Python instalado. 

Como começar
------------------------------
A sistema verifica automaticamente a existência da pasta ``dados``, se não existir ele a criará. Quando for realizada a primeira consulta, tenha ou não apostas para conferir, será criado o arquivo ``rcache.dat`` e seus auxiliares. Isso é absolutamente normal. 

Para começar basta um `prompt de comando` e acesso à internet.

Pronto? Então vamos começar pela :doc:`comandos`.    
    
.. seealso:: :doc:`prompt`.
    