﻿Interface de comandos
=========================================================

Os comandos podem ser escritos por extenso ou abreviado conforme ilustrado a seguir.

..  todo:: 
    Vou fazer uma tarefa.
    
Comandos e outras informações
--------------------------------------

.. toctree::
    :maxdepth: 1

    simula
    result
    resumo
    modals
    entidades

Abaixo está ilustrada a assinatura dos comandos. Em caso de dúvidas, consulte sempre o ``--help`` do sistema::

    >>> br [--modal] comando [-opções] [args]
    
    >>> br --help
    >>> br simula --help
    >>> br result --help
    >>> br Resumo --help

- ``[--modal]`` uma das modalidades de loteria. São autoexcludentes.    
- ``[-opções]`` opções disponíveis para cada comando.
- ``[args]`` argumentos variáveis para cada opção.

..  note:: Para os usuários do Windows e simpatizantes, as opções e modalidades podem ser precedidas pelas barras simples ``/`` e duplas ``//``, respectivamente.


    