﻿..  _topo:

Como fazer... 
============================

Existem apenas três comandos que fazem todo o trabalho. ``simula``, ``result`` e
``Resumo``, todos possuem aliases :mod:`argparse` e podem ser abreviados como: 
``s``, ``r`` e ``R``, respectivamente.

Se nenhum comando for informado, o sistema trará uma pequena informação sobre a
modalidade.

Se nenhuma modalidade for informada, o sistema assume a modalidade padrão **megasena**. Todas as informações são públicas e foram retiradas/adaptadas do site oficial da Caixa Econômica.

::

    >>> br
    ================================================================================
    Sobre a megasena
    ================================================================================
    Para realizar o jogo você deve marcar de 6 a 15 números dentre os 60 disponíveis
    no volante de apostas. Ganha o acertador dos 6 números sorteados, ou 4 ou 5
    números. Os sorteios são realizados duas vezes por semana, às quartas e aos
    sábados.

.. contents:: continue lendo...
    :depth: 2
    :local:

Simulação de apostas
---------------------------        

O comando mais simples retorna uma aposta da modalidade padrão aleatória.

::

    >>> br s
    ==========================================================================
    Lobras, Simulações da megasena                         19/09/2018-21:12:12
    ==========================================================================
    Aposta                  001                                    6 marcações
                                                             17-18-30-37-44-49
    --------------------------------------------------------------------------
     1 aposta(s) simulada(s)

..  topic:: nesse outro exemplo...
     
    ... o comando abaixo simula 4 apostas. 2 com 7 dezenas e 2 com 9 dezenas::

        Dezenas fixas ou da sorte: 10-20
        Quantidade: 2
        Número de dezenas: 7 e 9

            >>> br simula -d 10-20 -q 2 -n 7 9
            ==========================================================================
            Lobras, Simulações da megasena                         19/09/2018-21:13:34
            ==========================================================================
            Aposta                  001                                    7 marcações
                                                                  10-16-20-33-40-44-58
             - (2) dezena(s) fixa(s) [10-20]
            --------------------------------------------------------------------------
            Aposta                  002                                    7 marcações
                                                                  10-17-20-27-37-40-60
             - (2) dezena(s) fixa(s) [10-20]
            --------------------------------------------------------------------------
            Aposta                  003                                    9 marcações
                                                            01-09-10-19-20-22-23-31-50
             - (2) dezena(s) fixa(s) [10-20]
            --------------------------------------------------------------------------
            Aposta                  004                                    9 marcações
                                                            10-13-14-16-18-20-34-39-57
             - (2) dezena(s) fixa(s) [10-20]
            --------------------------------------------------------------------------
             4 aposta(s) simulada(s)
            
                
Conferência de apostas avulsas
---------------------------------------        

Obtendo o resultado do último concurso da modalidade padrão:

::

    >>> br r
    ==========================================================================
    Lobras, Resultado da megasena                          19/09/2018-21:36:56
    ==========================================================================
    Concurso                       ==>                                    2079
    Data do sorteio                ==>                          Ter 18/09/2018
    Acumulado                      ==>                        R$ 14.977.664,94
    --------------------------------------------------------------------------
    Resultado do sorteio           ==>                       01-02-14-37-55-58
    --------------------------------------------------------------------------
    Premiação
     sena                            0                                 R$ 0,00
     quina                          23                            R$ 43.305,35
     quadra                       1681                               R$ 846,45
    --------------------------------------------------------------------------
     - sem a postas para conferir
     - validade do concurso: Seg 17/12/2018
     - resultado obtido da internet, 92415ms


     
Obtendo o resultado de um concurso específico da modalidade padrão:

::

    >>> br result -c 2060
    ==========================================================================
    Lobras, Resultado da megasena                          19/09/2018-21:38:01
    ==========================================================================
    Concurso                       ==>                                    2060
    Data do sorteio                ==>                          Qua 18/07/2018
    Acumulado                      ==>                        R$ 56.029.903,37
    --------------------------------------------------------------------------
    Resultado do sorteio           ==>                       08-09-11-25-39-41
    --------------------------------------------------------------------------
    Premiação
     sena                            0                                 R$ 0,00
     quina                         186                            R$ 19.455,08
     quadra                       8570                               R$ 603,20
    --------------------------------------------------------------------------
     - sem a postas para conferir
     - validade do concurso: Ter 16/10/2018
     - resultado obtido do cache


     
Conferindo o resultado de duas apostas informadas manualmente, *com acerto*:

::

    >>> br r -c 2060 -d 15-19-25-26-30-39 08-09-11-25-39-45-59
    ==========================================================================
    Lobras, Resultado da megasena                          19/09/2018-21:38:39
    ==========================================================================
    Concurso                       ==>                                    2060
    Data do sorteio                ==>                          Qua 18/07/2018
    Acumulado                      ==>                        R$ 56.029.903,37
    --------------------------------------------------------------------------
    Resultado do sorteio           ==>                       08-09-11-25-39-41
    --------------------------------------------------------------------------
    Premiação
     sena                            0                                 R$ 0,00
     quina                         186                            R$ 19.455,08
     quadra                       8570                               R$ 603,20
    --------------------------------------------------------------------------
    Aposta                         001                             6 marcações
                                                             15-19-25-26-30-39
     - 2 acerto(s)
    --------------------------------------------------------------------------
    Aposta                         002                             7 marcações
                                                          08-09-11-25-39-45-59
     - Parabéns! aposta premiada (quina)
    --------------------------------------------------------------------------
     - 2 aposta(s) conferida(s)
     - validade do concurso: Ter 16/10/2018
     - resultado obtido do cache



topo_    

Conferência de um arquivo de apostas (pacote)
---------------------------------------------------        

..  seealso:: :doc:`planilhacsv`

Para conferir as apostas da **megasena** do arquivo **minhasapostas.csv** referentes ao concurso **2032**, digite:

::

    >>> br --megasena r -c 2032 -f minhasapostas.csv -V
    Buscando Concursos... 100% ●●●●●●●●●●●●●●●●●●●●
    ==========================================================================
    Lobras, Resultado da megasena                          19/09/2018-21:41:30
    ==========================================================================
    Concurso                       ==>                                    2032
    Data do sorteio                ==>                          Ter 17/04/2018
    Acumulado                      ==>                         R$ 5.308.080,78
    --------------------------------------------------------------------------
    Resultado do sorteio           ==>                       06-14-19-20-39-53
    --------------------------------------------------------------------------
    Premiação
     sena                            0                                 R$ 0,00
     quina                          27                            R$ 41.286,89
     quadra                       2075                               R$ 767,46
    --------------------------------------------------------------------------
    Aposta                         001                             6 marcações
                                                             02-19-26-33-47-50
     - 1 acerto(s)
    --------------------------------------------------------------------------
    Aposta                         002                             6 marcações
                                                             07-19-22-35-48-53
     - 2 acerto(s)
    --------------------------------------------------------------------------
     - 2 aposta(s) conferida(s)
     - validade do concurso: Seg 16/07/2018
     - resultado obtido do cache
     - atenção, concurso vencido (conferência forçada)


    
A diferença básica entre analisar uma aposta avulsa e um arquivo de apostas é a prévia seleção no arquivo, do material compatível com os parâmetros informados. Após essa etapa, o processamento é semelhante nas duas propostas.    
    
..  warning::
    Se não for informado o número do concurso, todas as apostas vinculadas a todos os concursos da modalidade serão conferidas. Isso pode demorar e tornar-se maçante, dependendo do volume de acessos individuais na internet.
    
..  note::
    Se o arquivo informado não existir, ou estiver vazio, ou não contiver apostas vinculadas ao concurso informado, o sistema retornará apenas o resultado do concurso informado. Se nenhum concurso for informado será retornado o último concurso.

topo_    
    
Obtendo o resumo do dia
---------------------------------------------------        

Para obter o resumo de todas as modalidades com informações gerenciais, digite:
    
::

    >>> br R -i
    Criando Resumo....... 100% ●●●●●●●●●●●●●●●●●●●●
    ==========================================================================
    Lobras, Resumo dos prêmios principais                  19/09/2018-21:44:30
    ==========================================================================
    Modalidade    Concurso Ganhadores         Pago/Acumulado   Próximo Sorteio
    --------------------------------------------------------------------------
    megasena        2079       0            R$ 14.977.664,94    Qui 20/09/2018
    lotomania       1902       1            R$ 11.484.774,85    Sex 21/09/2018
    timemania       1233       0             R$ 7.850.601,78    Qui 20/09/2018
    duplasena       1841       0             R$ 6.135.191,25    Qui 20/09/2018
    lotofacil       1713       3               R$ 796.502,25    Sex 21/09/2018
    quina           4780       1               R$ 647.397,85    Qui 20/09/2018
    diadesorte       52        1               R$ 485.937,23    Qui 20/09/2018
    federal        05320     08639             R$ 350.000,00    Sáb 22/09/2018
    loteca          818        49              R$ 299.340,12    Ter 25/09/2018
    lotogol         1014       0                R$ 46.091,14    Qui 20/09/2018
    --------------------------------------------------------------------------
     - resultado obtido da internet
     - a federal apresenta o número do bilhete do 1º prêmio
     - valores em azul representam os prêmios pagos
     - ordem decrescente de montante

Mais arquivos de exemplos na pasta exemplos.
    
topo_    
