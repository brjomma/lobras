﻿Dados da Caixa
========================

A Caixa não tem um padrão único de distribuição de informações de suas loterias. Cada modalidade vem de um jeito. Isso é típico do serviço público brasileiro e vai na contramão do programa `dados abertos <http://dados.gov.br/pagina/dados-abertos>`_ sobre a disponibilidade de informações pela administração pública. Mas tudo bem, dá-se um jeito.

Ainda assim, existem alguns que lucram com estas informações e fazem de tudo para retê-las. Felizmente também existem muitos outros que compartilham o que conseguem.

De qualquer maneira acho sensato facilitar a vida para quem encontrou alguma dificuldade. Abaixo está listado tudo que consegui reunir sobre o assunto. Existe um `aplicativo oficial de resultados <https://www.microsoft.com/pt-br/p/caixa-loterias/9wzdncrdgwpp>`_, mas é incompleto...

..  simple table:: 

+--------------+-------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+
|dados que são disponibilizados pela Caixa                                                                                                                                                                         |
+==============+=======================================================================================================+===========================================================================================+
|  megasena    | `Dados otimizados <http://www1.caixa.gov.br/loterias/loterias/megasena/megasena_pesquisa_new.asp>`_   | `Informações gerais <http://loterias.caixa.gov.br/wps/portal/loterias/landing/megasena>`_ |
+--------------+-------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+
|  duplasena   | `Dados otimizados <http://www1.caixa.gov.br/loterias/loterias/duplasena/duplasena_pesquisa_new.asp>`_ | `Informações gerais <http://loterias.caixa.gov.br/wps/portal/loterias/landing/duplasena>`_|
+--------------+-------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+
|  lotofacil   | `Dados otimizados <http://www1.caixa.gov.br/loterias/loterias/lotofacil/lotofacil_pesquisa_new.asp>`_ | `Informações gerais <http://loterias.caixa.gov.br/wps/portal/loterias/landing/lotofacil>`_|
+--------------+-------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+
|  lotomania   | `Dados otimizados <http://www1.caixa.gov.br/loterias/loterias/lotomania/_lotomania_pesquisa.asp>`_    | `Informações gerais <http://loterias.caixa.gov.br/wps/portal/loterias/landing/lotomania>`_|
+--------------+-------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+
|  quina       | `Dados otimizados <http://www1.caixa.gov.br/loterias/loterias/quina/quina_pesquisa_new.asp>`_         | `Informações gerais <http://loterias.caixa.gov.br/wps/portal/loterias/landing/quina>`_    |
+--------------+-------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+
|  timemania   | `Dados otimizados <http://www1.caixa.gov.br/loterias/loterias/timemania/timemania_pesquisa.asp>`_     | `Informações gerais <http://loterias.caixa.gov.br/wps/portal/loterias/landing/timemania>`_|
+--------------+-------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+
|  lotogol     | `Dados otimizados <http://www1.caixa.gov.br/loterias/loterias/lotogol/lotogol_pesquisa_new.asp>`_     | `Informações gerais <http://loterias.caixa.gov.br/wps/portal/loterias/landing/lotogol>`_  |
+--------------+-------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+
|  loteca      | `Dados otimizados <http://www1.caixa.gov.br/loterias/loterias/loteca/loteca_pesquisa_new.asp>`_       | `Informações gerais <http://loterias.caixa.gov.br/wps/portal/loterias/landing/loteca>`_   |
+--------------+-------------------------------------------------------------------------------------------------------+-------------------------------------------------------------------------------------------+


