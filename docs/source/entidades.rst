﻿Entidades
--------------------------------------
Entidades são pedaços autônomos da informação contida nos argumentos de cada opção. Podem ser isoladas ou encadeadas com qualquer caractere, com exceção da lotogol onde o separador obrigatório é o traço ``-``. Mas por definição, deve-se utilizar o traço ``-`` em todas e recomenda-se manter a consistência de utilizar apenas um tipo de caractere encadeador, caso contrário, os resultados serão imprevisíveis. 

Entidades devem ser separadas por espaço.

Entidades que representam bilhetes da loteria federal não usam caracteres separadores.

.. option:: dezenas <-d>

Podem ser isoladas ou encadeadas. Cada entidade obrigatoriamente gera uma aposta. Na modalidade federal o caractere separador é opcional.
    - uma ou mais entidades isoladas ``12`` ``54`` ...
    - uma ou mais entidades encadeadas ``23-44`` ``24-33-35`` ...
    - juntando entidades isoladas e encadeadas ``12`` ``54`` ``23-44`` ``24-33-35`` ...

.. option:: quantidade_de_dezenas <-n>

Não podem ser encadeadas. Cada entidade obrigatoriamente gera uma quantidade de apostas, correspondente ao seu valor.
    - uma ou mais entidades isoladas ``4`` ``8`` ...

.. option:: concurso <-c>

Podem ser isoladas ou representação de intervalo encadeado (a sequencia pode ser invertida). Cada entidade obriga a busca do seu respectivo número de concursos. No caso de intervalo, todos os números da sequência, inclusive, serão pesquisados. Qualquer caractere não numérico será simplesmente ignorado.
    - uma ou mais entidades isoladas ``400 450 300 499`` ...
    - uma ou mais entidades intervalos encadeados ``500-510`` ``600-595`` ...
    - juntando entidades isoladas e intervalos encadeados ``400 450 300 499`` ``500-510`` ``600-595`` ...
