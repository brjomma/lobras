..  include:: introducao.rst

Conteúdo
==============================

.. toctree::
   :maxdepth: 1

   leiame
   apresentacao
   comofazer
   comandos
   licenca
   Agradecimentos & contato <sobre>
   modulos
   
   
Índices e tabelas
=====================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
