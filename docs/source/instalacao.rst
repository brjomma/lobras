Instalação
--------------------------------

O sistema não é um utilitário instalável, portanto basta escolher um pasta de sua preferência e descarregar nela o conteúdo do pacote zipado. São vários arquivos, todos imprescindíveis, congelados com o utilitário `PyInstaller <https://www.pyinstaller.org/>`_, conserve-os como estão. Agora traga sua planilha de apostas para o subdiretório ``dados``, este é o seu arquivo mais importante para o qual recomendo o maior cuidado possível, pois na verdade trata-se do seu precioso banco de dados :doc:`planilhacsv`. Neste subdiretório já existe uma sugestão de modelo, adapte-a às suas necessidades.

Após isso, basta rodar o arquivo ``br.exe`` num *prompt de comando*.

Não existem outras dependências

.. seealso:: :doc:`prompt`.


