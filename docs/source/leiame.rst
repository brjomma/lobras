﻿Leia-me
==============================

Estas orientações cobrem a maior parte do projeto e esclarecem muitas dúvidas, foram adicionados alguns arquivos e imagens esclarecedores na pasta exemplos. Mesmo assim, restando dúvidas, mande-me uma mensagem.

Motivação
------------------------------

Conheci a linguagem há pouco mais de cinco meses e desde então passei a amar sua simplicidade e objetividade (`graças Guido <https://pt.wikipedia.org/wiki/Guido_van_Rossum>`_), assim tenho aprofundado os estudos na medida do possível. Tudo o que encontrei sobre conferência automática de loterias brasileiras não me atendia completamente e sempre ficava com meus bilhetes esquecidos no fundo de alguma gaveta.

Nada melhor do que aprender fazendo algo útil.

O que é isso
------------------------------

Um utilitário de brinquedo, testado no `MS Windows`, para conferir com certa rapidez e exatidão qualquer volume razoável de apostas lotéricas.

Como isso trabalha
------------------------------

O usuário deve informar preliminarmente, no :doc:`result` com os parâmetros das suas apostas para conferência, a entrada pode ser manual ou em arquivos no formato :mod:`csv`. Após um crítica interna o sistema consulta o concurso informado, no cache local ou na internet e faz a comparação das dezenas apostadas e sorteadas. Em seguida imprime um relatório na tela e, opcionalmente, salva e exibe um arquivo `html`.

..  note::  Quando a informação do concurso está ausente ``-c``, o sistema sempre buscará o último concurso na internet. Nesse caso, a conferência visual do número do concurso correto será responsabilidade do usuário.

Para o :doc:`simula` nunca haverá consultas externas.

Se você gosta de gerenciar diariamente, use o :doc:`resumo` com a diretiva ``-i`` apenas na primeira vez.

..  include:: instalacao.rst

Exemplos
------------------------------

Detalhes em :doc:`comofazer`

Outros arquivos de exemplos encontram-se na pasta `exemplos`. No `prompt de comando` consulte sempre o help ``-h``.

Construído com
------------------------------
- `Python <http://python.org/>`_.

Testes
------------------------------

O sistema foi testado muitas vezes durante um tempo razoável, mas nada garante a sua completa exatidão. Os procedimentos (todos ou individualmente) encontram-se na pasta `testes`, para excutá-los você precisará clonar o repositório e ter o Python instalado. 

Contribuindo
------------------------------

Para contribuir, por favor, entre em contato e observe que apreciamos o `Pacto do Colaborador <https://www.contributor-covenant.org/pt-br/version/1/4/code-of-conduct>`_, para normas de conduta, amabilidade e respeito ao próximo.

Histórico
------------------------------

versão (0.1.0), lançamento.

Autor 
------------------------------

- `@brjomma <http://telegram.me/brjomma>`_.
- e-mail: brjomma@gmail.com

:ref:`mais detalhes <autor>`

Licença
------------------------------

Veja :doc:`licenca` para detalhes.

..  include:: agradecimentos.rst
