﻿Modalidades
----------------------------------------

A `modalidade da loteria <http://loterias.caixa.gov.br/wps/portal/loterias>`_ define suas particularidades como: número mínimo e máximo de dezenas, etc. Deve ser informada logo no início e antes do comando desejado.

Até onde não houver conflito de ambiguidade, pode ser abreviada a partir da primeira letra. Ex: --m, --me, --meg, --mega, etc...

    >>> br [--modal]

Veja algumas informações básicas. Mais informações podem ser obtidas direto no `site da Caixa <http://loterias.caixa.gov.br/wps/portal/loterias>`_.

.. option:: --megasena

    - dezenas mínimas: 6
    - dezenas máximas: 15
    - display de numeros: 1 a 60
    - premiação: sena, quina, quadra

.. option:: --duplasena

    Esta modalidade é a única que tem dois sorteios.
    
    - dezenas mínimas: 6
    - dezenas máximas: 15
    - display de numeros: 1 a 60
    - premiação: sena, quina, quadra e terno
    
.. option:: --diadesorte

    - dezenas mínimas: 7
    - dezenas máximas: 15
    - display de numeros: 1 a 31
    - premiação: 7, 6, 5 e 4 acertos e o 'mês da sorte'
    - mês da sorte: 1 a 12    
    
.. option:: --lotofacil

    - dezenas mínimas: 15
    - dezenas máximas: 18
    - display de numeros: 1 a 25
    - premiação: 15, 14, 13, 12 e 11 acertos
        
.. option:: --lotomania

    O string com o número ``100`` será representado pela dezena ``00`` para fins de compatibilidade com o resultado oficial, mas nas
    rotinas internas, continuará sendo o mesmo.

    - dezenas mínimas: 50
    - dezenas máximas: 50
    - display de numeros: 1 a 100
    - premiação: 20, 19, 18, 17, 16, 15 e 0  acertos

    
.. option:: --quina

    - dezenas mínimas: 5
    - dezenas máximas: 15
    - display de numeros: 1 a 80
    - premiação: quina, quadra, terno e duque

.. option:: --timemania

    - dezenas mínimas: 10
    - dezenas máximas: 10
    - display de numeros: 1 a 80
    - premiação: 7, 6, 5, 4 e 3 acertos e o 'time do coração'

.. option:: --lotogol

    - marcações mínimas: 5
    - marcações máximas: 5
    - display de numeros: 0, 1, 2, 3, + e suas combinações
    - premiação: 5, 4 e 3 acertos

.. option:: --loteca

    - marcações mínimas: 14
    - marcações máximas: 14
    - display de numeros: 0, 1, 2 e suas combinações
    - premiação: 14 e 13 acertos

.. option:: --federal

    - dígitos mínimos: 5
    - dígitos máximos: 5
    - display de numeros: 1 a 91000
    - premiação: 1º, 2º, 3º, 4º e 5º prêmios e seus derivados


    