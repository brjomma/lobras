﻿Módulos do Projeto
========================

Documentação do código fonte.

.. autosummary::
   :toctree: _autosummary

   resumo
   resultados
   sorteios
   simulacoes
   apostadas
   simuladas
   documento
   console
