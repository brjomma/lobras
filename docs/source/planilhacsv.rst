﻿Criando um arquivo CSV
============================

Você pode usar qualquer software de planilha de sua preferência, estamos assumindo a utilização do Libreoffice Calc. Selecione ``Arquivo/Novo`` e crie uma planilha comum conforme o modelo abaixo, depois salve, por conveniência, no subdiretório ``dados`` da pasta de trabalho:

..  simple table:: 

+------------+------+----+----+----+----+----+----+-----+-----+
| modalidade | conc | d1 | d2 | d3 | d4 | d5 | d6 | ... | d50 |
+------------+------+----+----+----+----+----+----+-----+-----+

..  centered::
    .
    .
    .
    
:sobre as colunas:

    - ``modalidade`` Escreva o nome da modalidade, na dúvida digite no *prompt*:

        >>> br -h
        
    - ``conc`` Escreva o número do concurso. Se a aposta tiver ``Teimosinha``, preencha tantas linhas quantas forem os concursos.
    - ``d1...d50`` Colunas das dezenas. A tabela acima mostra apenas o cabeçalho, as dezenas reais não tem a letra *d*.
    
Lembre-se de que uma aposta mínima da quina são 5 dezenas, mas uma da lotomania requer a marcação e conferência de 50 dezenas.

Terminada a edição e salvamento, vá no menu  ``Arquivo/Salvar como...`` e escolha o tipo ``Texto CSV`` e clique em ``Salvar``. Confirme o formato do arquivo clicando no botão ``Utilizar o formato Texto CSV`` no quadro que aparece e ``OK`` no próximo quadro. Pronto, agora você tem dois arquivos, o original ``odf`` e o novo arquivo ``CSV``. Este último será o arquivo de trabalho do sistema.

..  tip::

    Preserve a planilha-mestre no formato original para permitir o seu controle e futuras manipulações.
        
..  warning::

    * É inútil manipular o arquivo ``CSV`` pois ele será sobrescrito no futuro. Use a sua planilha original para fazer as alterações necessárias e repita o procedimento sempre que necessário.
    * A perfeição da conferência depende diretamente dos dados informados no arquivo. Por isso, tenha a máxima atenção ao impostá-los.