﻿Criando um prompt no Windows
===========================================

Para criar um atalho permanente, vá até ``C:\Windows\system32`` e clique com o botão direito do mouse em ``cmd.exe`` e selecione ``Criar atalho``. Diga ``sim`` para a área de trabalho.

Agora, para personalizar, clique em cima do atalho recém-criado novamente com o botão direito e altere o caminho ``iniciar em:`` para a pasta que você escolheu para o sistema.

Pronto, o atalho é uma janela como outra qualquer, agora sempre que quiser usar o programa basta clicar no atalho e digitar o comando desejado na janela que aparecerá.

..  tip::

    Para executar o mesmo comando em modo solo você deve abrir o *Prompt de Comando (⊞ Win-r)*, digitar *cmd* e *Enter*, depois digite *cd + caminho completo da pasta de trabalho*.
    









    