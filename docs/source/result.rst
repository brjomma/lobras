﻿..  |resumo| replace:: :class:`resumo <resumo.Resumo>`
..  |resultado| replace:: :class:`resultado <resultados.Pacote>`
..  |aposta| replace:: :class:`apostas <apostadas.Apostada>`


Comando **result** ou **r**
===================================================================

Simula um ou mais apostas da modalidade informada. Para determinar as dezenas do coração ou da sorte, quantidades de apostas e quantidade de dezenas, utilize as opções isoladamente ou em conjunto.

    >>> br [--modal] result [opções] [args]

A simulação de bilhetes da loteria federal realmente faria sentido se você pudesse comprá-los escolhendo seu número preferido, o que é praticamente impossível pois eles são espalhados pelo país semanalmente.

A simulação de marcações da loteca é pífia pois são apenas três escolhas possíveis, mas mesmo assim fica disponível.

[opções]
----------------------------------------
Desde que estejam acompanhadas de seus respectivos argumentos as opções podem ser informadas em qualquer ordem. 

.. option:: --help, -h

    exibe as opções disponíveis

.. option:: -d <entidade...>

    :class:`list` [:class:`str`] Define a :doc:`entidade <entidades>` `dezenas`, ou seja, as dezenas que representam uma ou mais apostas avulsas para conferência.
    
    ..  note::
    
        lembre-se: na lotomania o número ‘100’ é representado como ‘00’. Mas trate-o como ‘100’. Ex: -d 100

.. option:: -f <arquivo...>

    :class:`list` [:class:`str`] Espera um ou mais nomes de arquivos contendo as suas |aposta|. Quando houver mais de um concurso apresenta uma linha de resumo. O sistema espera encontrar os arquivos no subdiretório ``dados``.
    
.. option:: -c <entidade...>

    :class:`list` [:class:`str`] Define a :doc:`entidade <entidades>` `concurso`, pode ser um número simples ou uma representação de intervalo separada por um caractere `não numérico`. A ausência da informação do concurso equivale a uma diretiva com a opção ``-i``, pois o sistema não tem como saber qual o concurso desejado e sempre vai consultar o último sorteado. Cuidado com intervalos muito longos.

.. option:: -i

    :class:`bool` Força a consulta na internet e sobrepõe as informações contidas no ``cache``.
    
.. option:: -t

    :class:`int` Determina o `time do coração` ou o `mês da sorte` nas modalidades :class:`timemania <simuladas.Timemania>` e  :class:`diadesorte <simuladas.Diadesorte>`. É ignorada em outras modalidades.
    Veja a `tabela de correlação <http://www.caixa.gov.br/downloads/loterias-timemania/150306_Matriz%20Times%20TIMEMANIA.pdf>`_ "Número" x "Time do Coração" .

    Existe outra maneira de produzir as informações acima e são utilizadas por padrão nos arquivos de lote. basta acrescentar um número correspondente ao `time do coração` ou o `mês da sorte` no final da cadeia de dezenas.
    
    .. warning::
        Certifique-se da consistência da informação dessa segunda alternativa, caso contrário o resultado será enganoso.
        
.. option:: -H 

    :class:`bool` Salva a simulação em um arquivo *html* e o exibe no browser padrão.

.. option:: -u

    :class:`bool` Individualiza cada concurso em um arquivo *html* separado. Não use isso para um grande número de concursos.

.. option:: -X

    :class:`bool` Deleta o cache existente antes da consulta. É aconselhável a limpeza periódica do cache, isso ajuda a manter o sistema leve e ágil nas consultas. Igualmente benéfico é manter sua planilha livre de concursos vencidos, a não ser que seja estritamente necessário.
