﻿..  |resumo| replace:: :class:`resumo <resumo.Resumo>`
..  |resultado| replace:: :class:`resultado <resultados.Pacote>`
..  |aposta| replace:: :class:`apostas <apostadas.Apostada>`


Comando **Resumo** ou **R**
===================================================================

Para quem gosta de gerenciar. Tenho absoluta certeza que esta é uma ferramenta *não esperada*, devido sua relevância. Uma das possíveis mensagens do rodapé é o aviso de premiação. Espero que você receba muitas delas.

    >>> br Resumo [opções] [args]

Aqui a brincadeira fica mais interessante. Uma simples consulta diária lhe dará a visão gerencial de todas as modalidades e apontará as alternativas mais vantajosas para efetuar apostas. É recomendado que na primeira consulta diária seja adicionada a opção ``-i``, após isso, tudo estará localmente no cache.

Utilizar este comando em conjunto com o `site de apostas <http://loteriasonline.caixa.gov.br/>`_ on-line tornará a brincadeira ainda melhor.

[opções]
----------------------------------------
Desde que estejam acompanhadas de seus respectivos argumentos as opções podem ser informadas em qualquer ordem. 

.. option:: --help, -h

    exibe as opções disponíveis.

.. option:: -a

    :class:`bool` Ordena o |resumo| alfabeticamente.

.. option:: -p

    :class:`bool` Ordena o |resumo| pela data do próximo concurso.

.. option:: -f <arquivo...>

    :class:`list` [:class:`str`] Espera um ou mais nomes de arquivos contendo as suas |aposta|. Em seguida apresenta adicionalmente, de forma gerencial, os concursos e apostas pendentes de conferência. O sistema espera encontrar os arquivos no subdiretório ``dados``.
    
    Imprime o |resumo| e o |resultado| das pendências.

.. option:: -r

    :class:`bool` Executa adicionalmente a consulta do último concurso de cada modalidade. As consultas seguintes serão realizadas no cache até que seja forçada nova consulta na internet através da opção ``-i`` ou o cache seja deletado, depois imprime o |resumo| e o |resultado| de sorteios.
    
.. option:: -n

    :class:`bool` Executa adicionalmente a conferência de todas as |aposta| de todas as modalidades presentes nos arquivos e depois imprime o |resumo|, as pendências e o |resultado| de sorteios juntamente com a conferência total.

    .. warning::
        Esta é uma diretiva delicada pois ela vai varrer os arquivos e coletar todas as apostas. Se o arquivo ``rcache.dat`` for deletado ou a consulta do sorteio for forçada na internet através da opção ``-i``, torna-se uma opção demorada e poderá comprometer o funcionamento da máquina, dependendo do tamanho dos seus arquivos. 

.. option:: -H

    :class:`bool` Salva a simulação em um arquivo *html* e o exibe no browser padrão. O |resumo| sempre será salvo em arquivo único.

.. option:: -i

    :class:`bool` Força a consulta na internet e sobrepõe as informações contidas no ``cache``.
    
.. option:: -V

    :class:`bool` Força a conferência de concursos :ref:`vencidos <vencidos>`.

