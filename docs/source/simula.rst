﻿Comando **simula** ou **s**
===================================================================

Simula um ou mais apostas da modalidade informada. Para determinar as dezenas do coração ou da sorte, quantidades de apostas e quantidade de dezenas, utilize as opções isoladamente ou em conjunto.

    >>> br [--modal] simula [opções] [args]

A simulação de bilhetes da loteria federal realmente faria sentido se você pudesse comprá-los escolhendo seu número preferido, o que é praticamente impossível pois eles são espalhados pelo país semanalmente.

A simulação de marcações da loteca é pífia pois são apenas três escolhas possíveis, mas mesmo assim fica disponível.

[opções]
----------------------------------------
Desde que estejam acompanhadas de seus respectivos argumentos as opções podem ser informadas em qualquer ordem. 

.. option:: --help, -h

    exibe as opções disponíveis

.. option:: -d <entidade...>

    :class:`list` [:class:`str`] Define a :doc:`entidade <entidades>` `dezenas`, ou seja, as suas dezenas/marcações preferidas, ou como preferir, dezenas da sorte ou do coração. Essa informação será mantida na geração de marcações ou dezenas aleatórias. Não faz muito sentido em apostas da loteca ou nos bilhetes da federal.

.. option:: -n <entidade...>

    :class:`list` [:class:`int`] Determina a quantidade de dezenas a simular. Obedece aos valores mínimos e máximos de cada modalidade.
    Nas loterias esportivas (:class:`lotogol <simuladas.Lotogol>` e :class:`loteca <simuladas.Loteca>`) a informação da quantidade de dezenas é inútil pois os valores mínimos e máximos são inflexíveis.

.. option:: -q

    :class:`int` Determina a quantidade de apostas a simular. Considera a quantidade de dezenas e a quantidade de números de dezenas e as multiplica.

.. option:: -t

    :class:`int` Determina o ``time do coração`` ou o ``mês da sorte`` nas modalidades :class:`timemania <simuladas.Timemania>` e :class:`diadesorte <simuladas.Diadesorte>`. É ignorada em outras modalidades.
    Veja a `tabela de correlação <http://www.caixa.gov.br/downloads/loterias-timemania/150306_Matriz%20Times%20TIMEMANIA.pdf>`_ "Número" x "Time do Coração" .

.. option:: -H

    :class:`bool` Salva a simulação em um arquivo *html* e o exibe no browser padrão.

.. option:: -D

    :class:`int` Determina o número de **duplos** na simulação de apostas da :class:`loteca <simuladas.Loteca>`. É ignorada em outras modalidades. Obedece aos limites oficiais da modalidade.
    Outra alternativa que sobrepõe esta opção é informar os **duplos** diretamente nas marcações. Ex: ``-d 20``

.. option:: -T

    :class:`int` Determina o número de **triplos** na simulação de apostas da :class:`loteca <simuladas.Loteca>`. É ignorado em outras modalidades. Obedece aos limites oficiais da modalidade.
    Outra alternativa que sobrepõe esta opção é informar os **triplos** diretamente nas marcações. Ex: ``-d 102``

