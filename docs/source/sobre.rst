Contato
======================

..  include:: agradecimentos.rst

..  _autor:

Sobre o autor
-----------------

Sou apenas um autodidata entusiasmado do `Python <http://python.org/>`_, não desenvolvedor de carreira. Conheci a linguagem há pouco mais de cinco meses e desde então tenho aprofundado os estudos na medida do possível. Tudo o que garimpei sobre conferência automática de loterias brasileiras não me atendia completamente e daí sempre ocorria o esquecimento dos meus bilhetes no fundo de alguma gaveta.

Agora vivo entusiasmado e com vontade de jogar mais só para ver as apostas sendo automaticamente analisadas. Epa! Isso é um perigo.

Geralmente não publico meus estudos, no entanto acho que a utilidade deste sisteminha pode servir a alguém com as mesmas dificuldades que eu tive. Assim, os interessados são livres para aproveitar o que acharem razoável e útil, ou fazer alguma sugestão.

Um esclarecimento oportuno
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Este Sistema efetua a conferência dos :doc:`dez tipos de loterias da Caixa <modals>`. Minha primeira tentativa foi fazer um software gráfico usando o `PyQt <http://pyqt.sourceforge.net/Docs/PyQt5/>`_. Avancei bastante, mas num certo ponto, a curva de aprendizado ficou pesada e percebi que não conseguiria terminar a contento, assim optei por um programa leve, de linha de comando. Mais rápido e mais prático para a tarefa. O visual gráfico ficará para outra oportunidade.

..  note:: 

    Em meados de Agosto, quando eu já terminava a versão preliminar, a Caixa lançou seu `site de apostas <http://loteriasonline.caixa.gov.br/>`_ on-line. Parabéns para eles, pois lá é possível apostar em várias modalidades (não em todas ainda) e conferir as apostas com segurança. Mas é limitado ao que foi feito pelo site. Qual a nossa vantagem? Aqui você é o dono das suas apostas e brinca com elas em segurança, pois nada é movimentado pela internet. O resto deixo para você mesmo tirar suas conclusões.

Contato
-----------------

Contatos com o autor podem ser feitos através de:

- e-mail: brjomma@gmail.com
- `@brjomma <http://telegram.me/brjomma>`_
- repo: https://bitbucket.org/brjomma/lobras
- repo: https://gitlab.com/brjomma/lobras
