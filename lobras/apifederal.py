#!/usr/bin/python
# coding: utf-8

__version__ = '0.1'

__copyright__ = 'Copyright 2018, @brjomma <brjomma@gmail.com>'
__license__ = 'MIT <https://mit-license.org/>'


class Bilhetes(object):
    def __init__(self, nbil):
        self.bilhete = nbil
        self.dezena = self.bilhete[-2:]
        self.centena = self.bilhete[-3:]


class Premio(Bilhetes):
    def __init__(self, args, nbil):
        super().__init__(nbil)
        self._milhar = self.bilhete[-4:]
        self.derivados = []
        
        self._obt_derivados()


class Principal(Premio):
    def __init__(self, args, nbil):
        super().__init__(args, nbil)
        self.unidade = self.bilhete[-1]
        self.adjacentes = []
        self.especiais = [str(int(self.bilhete) - 1), str(int(self.bilhete) + 1)]
        self._obt_adjacentes()
        
    def _obt_derivados(self):
        for r in range(90):
            self.derivados.append(str(r).zfill(2) + self.centena)
        
    def _obt_adjacentes(self):
        self.adjacentes.append(str(int(self.dezena) - 3))
        self.adjacentes.append(str(int(self.dezena) - 2))
        self.adjacentes.append(str(int(self.dezena) - 1))
        self.adjacentes.append(str(int(self.dezena) + 1))
        self.adjacentes.append(str(int(self.dezena) + 2))
        self.adjacentes.append(str(int(self.dezena) + 3))


class Secundario(Premio):
    def __init__(self, args, nbil):
        super().__init__(args, nbil)
        
    def _obt_derivados(self):
        for r in range(10):
            if int(str(r) + self._milhar) < 90000: #self._qbilhetes
                self.derivados.append(str(r) + self._milhar)


class Bilhete(Bilhetes):
    ordens = '2\u00BA', '3\u00BA', '4\u00BA', '5\u00BA'
    def __init__(self, nbil, sorteados):
        super().__init__(nbil)
        self.unidade = self.bilhete[-1]
        self.sorteados = sorteados
        self.pprin = None
        self.secs = []
        self.msg = None
        self.acerto = []
        self.ordem = 0
        self.premiado = False
        
        self._obt_secundarios()
        self._confere_premio()
        
    def _confere_premio(self):
        self.premiado = self._conf_bilhete() or self._conf_derivado() or self._conf_centena() or self._conf_dezena() or self._conf_unidade()
        
    def _obt_secundarios(self):
        if len(self.sorteados):
            self.pprin = Principal('args',self.sorteados[0])
            for nsort in self.sorteados[1:]:
                self.secs.append(Secundario('args', nsort))

    def _conf_bilhete(self):
        ordem = None
        if self.bilhete == self.pprin.bilhete:
            ordem = '1\u00BA'
            self.ordem = 0
        else:
            for n, sec in enumerate(self.secs):
                if self.bilhete == sec.bilhete:
                    ordem = __class__.ordens[n]
                    self.ordem = n + 1 
                    break

        if ordem is not None:
            self.msg = ' - Parabéns! bilhete ganhou {} prêmio'.format(ordem)
            return True

    def _conf_derivado(self):
        ordem = None
        if self.bilhete in self.pprin.derivados + self.pprin.especiais:
            ordem = '1\u00BA'
            self.ordem = 0
            self.acerto = [dg for dg in self.bilhete]
        else:
            for n, sec in enumerate(self.secs):
                if self.bilhete in sec.derivados:
                    ordem = __class__.ordens[n]
                    self.ordem = n + 1
                    self.acerto = [dg for dg in sec.bilhete]
                    break

        if ordem is not None:
            self.msg = ' - Parabéns! bilhete derivado {} prêmio'.format(ordem)
            return True
    
    def _conf_centena(self):
        for n, sec in enumerate(self.secs):
            if self.centena == sec.centena:
                self.msg = ' - Parabéns! centena derivada {} prêmio'.format(__class__.ordens[n])
                self.ordem = n + 1
                self.acerto = [dg for dg in sec.bilhete]
                return True

    def _conf_dezena(self):
        ordem = None
        if self.dezena == self.pprin.dezena or self.dezena in self.pprin.adjacentes:
            ordem = '1\u00BA'
            self.ordem = 0
            self.acerto = [dg for dg in self.pprin.bilhete]
        else:
            for n, sec in enumerate(self.secs):
                if self.dezena == sec.dezena:
                    ordem = __class__.ordens[n]
                    self.ordem = n + 1
                    self.acerto = [dg for dg in sec.bilhete]

        if ordem is not None:
            self.msg = ' - Parabéns! dezena derivada {} prêmio'.format(ordem)
            return True

    def _conf_unidade(self):
        if self.unidade == self.pprin.unidade:
            self.msg = ' - Parabéns! unidade derivada 1\u00BA prêmio'
            self.acerto = [dg for dg in self.pprin.bilhete]
            self.ordem = 0
            return True


