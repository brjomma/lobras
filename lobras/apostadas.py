﻿#!/usr/bin/python
# coding: utf-8
"""Objetos que definem os números de conferência.

Os objetos assumem  características diferentes de acordo com a modalidade.

"""

__version__ = '0.1.0'

__copyright__ = 'Copyright 2018, @brjomma <brjomma@gmail.com>'
__license__ = 'MIT <https://mit-license.org/>'


import random
from lxml.builder import E #elementMaker
import utils as ut
import apifederal
import constantes as ct
import modals


BLOCO = 'display: inline-block;'


class Apostada(object):
    """Aposta que terá suas dezenas conferidas e informará os acertos.
    
    :param str flat_dezenas: Uma entidade encadeada de strings representando as dezenas da aposta. ``'15-16-19-25-26-25'``.
    :param sorteio: objeto :class:`sorteios.Sorteio` que contém os dados sorteados. Cada aposta pode estar vinculada a um ou mais sorteios (Teimosinha).    
    """
    def __init__(self, flat_dezenas, sorteio):
        self._sorteio = sorteio
        Obj = objs[self._sorteio.indexmodal]
        self.dezenas = Obj(flat_dezenas=flat_dezenas, sorteio=sorteio)
        self.dezenas.conferir()

    def montatela(self, na):
        '''
        Produz uma lista com fragmentos de dados de impressão da simulação.
        
        :param int na: Um numeral representando a ordem da aposta corrente.
        :return: :class:`list`.
        
        Será incorporada à :class:`console <console.Console>` principal.
        '''
        
        frag = []
        frag.append((('Aposta', 24),
                    (str(na+1).zfill(3), 10, 'dir'),
                    {'texto':ct.INFO_MARCACOES.format(len(self.dezenas.numeros)), 
                    'alin':'dir'}))
        
        [frag.extend(self.dezenas.montatela(ns, na)) for ns, _ in enumerate(self._sorteio.sorteadas)]
                
        return frag

    def montadoc(self, na):
        '''
        Produz uma lista com fragmentos de dados html* referente a aposta.
        
        :param int na: Um numeral representando a ordem da aposta corrente.
        :return: :class:`list`.
        
        Será incorporado ao :class:`documento <documento.Doc>` principal.
        '''
        
        frag = E.div({'class':'aposta apostada'},
                        E.table(
                            E.thead(
                                E.tr(E.th('Aposta'), 
                                      E.th(str(na+1).zfill(3)),
                                      E.th(ct.INFO_MARCACOES.format(len(self.dezenas.numeros))) )),
                            E.tbody(*self.dezenas.montadoc()),
                            E.tfoot(*[E.tr(E.th(msg, colspan='3')) for msg in self.dezenas._msgs])
                                )
                        )
        return frag

        
class Dezenas(modals.Dezenas, object):
    """Objetos que serão comparados e vão gerar um relatório de acertos.

    :param str args: A modalidade da loteria.
    :param str flat_dezenas: Uma cadeia de strings representando as dezenas da aposta. ``"15-16-19-25-26-25"``.
    :param int ndez: Um numeral com o valor da quantidade de dezenas.
    """

    def __init__(self, **kwargs):
        super().__init__()
        self._sorteio = kwargs['sorteio']
        self._dvargs = self._sorteio.dvargs
        self._modal = self._dvargs['modal']
        self._flatdezenas = kwargs.get('flat_dezenas')  #[None]
        self._acertadas = []
        self._msgs = []
        self._lst_prevalidas = []
        self.numeros = []

        self._fatiar()
        self._prevalidar()
        self._confere_display()

    def _conta_acertos(self, ns):
        """Conta os acertos em cada sorteio."""
        
        return len(self._acertadas[ns])

    def _confere_display(self):
        if len(self._lst_prevalidas) not in range(self.__class__.min, self.__class__.max+1):
            self._msgs.append(ct.MSG_NUM_DEZ_INVALIDO_VER.format(len(self._lst_prevalidas), self.__class__.min, self.__class__.max))
            
        # dezenas numéricas devem ser ordenadas
        #
        self.numeros.sort(key=int)

    def conferir(self):
        for sorteadas in self._sorteio.sorteadas:
            self._acertadas.append(list(set(self.numeros) & set(sorteadas)))

    def montatela(self, ns, na):
        """Produz uma lista com fragmento com linhas de impressão das dezenas apostadas.

        Este fragmento será incorporado ao objeto :class:`console <console.Console>` principal.

        :param int ns: Número do sorteio.
        :param int na: Número da aposta.
        :return: :class:`list`
        """

        frag = []
        num_acertos = self._conta_acertos(ns)
        str_sort = 'Sorteio {}: '.format(ns+1) if len(self._acertadas) > 1 else ''

        frag.append((dict(texto='-'.join([ut.verde(dz) if dz in self._acertadas[ns] else dz for dz in self.numeros]), alin='dir'),))

        if num_acertos in self.__class__.pontuacao:
            i = self.__class__.pontuacao.index(num_acertos)
            frag.append((ut.verde(ct.MSG_APOSTA_PREMIADA.format(str_sort, self._sorteio.premiacao[i])),))
            self._sorteio.premiadas.append((self._sorteio.modal, self._sorteio.concurso, na+1, 
                                            self._sorteio.premiacao[i], self._sorteio.rateios[ns][i][1]))
        else:
            frag.append((ct.MSG_ACERTOS.format(str_sort, num_acertos),))

        [frag.append((msg,)) for msg in self._msgs]
        return frag
        
    def montadoc(self):
        """Produz uma lista de fragmentos *html* das dezenas apostadas.

        Estes fragmentos serão incorporados ao objeto :class:`documento <documento.Doc>` principal.

        :return: :class:`list`
        """

        frag = []
        for ns, _ in enumerate(self._acertadas):
            num_acertos = self._conta_acertos(ns)
            str_sort = 'Sorteio {}: '.format(ns+1) if len(self._acertadas) > 1 else ''
            frag.append(E.tr(E.td(E.ul(*[E.li(dz, {'class':'acerto'} if dz in self._acertadas[ns] else {'class':'normal'}, style=BLOCO) for dz in self.numeros],
                                               {'class':'ul_apostadas ' + self._modal}), colspan='3')))

            if num_acertos in self.__class__.pontuacao:
                i = self.__class__.pontuacao.index(num_acertos)
                frag.append(E.tr({'class':'msg acerto'}, E.td(ct.MSG_APOSTA_PREMIADA.format(str_sort, self._sorteio.premiacao[i]), colspan='3')))
            else:
                frag.append(E.tr({'class':'msg'}, E.td(ct.MSG_ACERTOS.format(str_sort, num_acertos), colspan='3')))

        return frag


class Lotogol(modals.Lotogol, Dezenas):
        
    def _confere_display(self):
        if len(self._lst_prevalidas) != self.__class__.max:
            self._msgs.append(ct.MSG_MARC_MIN_MAX.format(len(self._lst_prevalidas), self.__class__.min, self.__class__.max))

    def conferir(self):
        for sorteadas in self._sorteio.sorteadas:
            self._acertadas.append([x for x, y in zip(self.numeros, sorteadas) if y in x])

    def montatela(self, ns, na):
        frag = []
        num_acertos = self._conta_acertos(ns)
        str_sort = 'Sorteio {}: '.format(ns+1) if len(self._acertadas) > 1 else ''

        if len(self.numeros) > len(self._sorteio.sorteadas[ns]):
            diff = self.numeros[len(self._sorteio.sorteadas[ns]):]
        else:
            diff = []

        frag.append((dict(texto='-'.join([ut.verde(x) if y in x else x for x, y in zip(self.numeros, self._sorteio.sorteadas[ns])] + diff), alin='dir'),))

        if num_acertos in self.__class__.pontuacao:
            i = self.__class__.pontuacao.index(num_acertos)
            frag.append((ut.verde(ct.MSG_APOSTA_PREMIADA.format(str_sort, self._sorteio.premiacao[i])),))
            self._sorteio.premiadas.append((self._sorteio.modal, self._sorteio.concurso, na+1, 
                                            self._sorteio.premiacao[i], self._sorteio.rateios[ns][i][1]))
        else:
            frag.append((ct.MSG_ACERTOS.format(str_sort, num_acertos),))

        [frag.append((msg,)) for msg in self._msgs]
        return frag
        
    def montadoc(self):
        frag = []
        for ns, _ in enumerate(self._acertadas):
            num_acertos = self._conta_acertos(ns)
            str_sort = 'Sorteio {}: '.format(ns+1) if len(self._acertadas) > 1 else ''
            
            frag.append(E.tr(E.td(E.ul(*[E.li(x, {'class':'acerto'} if y in x else {'class':'normal'}, style=BLOCO) for x, y in zip(self.numeros, self._sorteio.sorteadas[ns])], {'class':'ul_apostadas ' + self._modal}), colspan='3')))
            
            if num_acertos in self.__class__.pontuacao:
                i = self.__class__.pontuacao.index(num_acertos)
                frag.append(E.tr({'class':'msg acerto'}, E.td(ct.MSG_APOSTA_PREMIADA.format(str_sort, self._sorteio.premiacao[i]), colspan='3')))
            else:
                frag.append(E.tr({'class':'msg'}, E.td(ct.MSG_ACERTOS.format(str_sort, num_acertos), colspan='3')))

        return frag


class Loteca(modals.Loteca, Lotogol):

    def _confere_display(self):
        # if len(self._lst_prevalidas) != self.__class__.max:
            # self._msgs.append(ct.MSG_MARC_MIN_MAX.format(len(self._lst_prevalidas), self.__class__.min, self.__class__.max))
            
        # da Lotogol
        super()._confere_display()
        sucesso, msg = self._valida_extras(self._cont_duplos(), self._cont_triplos())
        if msg:
            self._msgs.append(msg)

    def _cont_duplos(self):
        return len([x for x in self.numeros if len(x) == 2])

    def _cont_triplos(self):
        return len([x for x in self.numeros if len(x) == 3])


class Megasena(modals.Megasena, Dezenas):
    pass


class Duplasena(modals.Duplasena, Dezenas):
    pass

    
class Diadesorte(modals.Diadesorte, Dezenas):

    def _prevalidar(self):
        # no arquivo de apostas o time do coração é sempre a ultima dezena informada
        # na informação manual é dado no parametro "-t"
        #
        super()._fatiar()
        self._obt_mesdasorte(self._dvargs.get('t') or self._lst_dezenas.pop())
        super()._prevalidar()

    def conferir(self):
        # for sorteadas in self._sorteio.sorteadas:
            # self._acertadas.append(list(set(self.numeros) & set(sorteadas)))
        super().conferir()
        self._acertou_mesdasorte = self._mesdasorte == self._sorteio._extra[1]

    def montatela(self, ns, na):
        frag = []
        num_acertos = self._conta_acertos(ns)
        str_sort = 'Sorteio {}: '.format(ns + 1) if len(self._acertadas) > 1 else ''

        frag.append((' - seu mês da sorte', dict(texto=str(self._mesindex) + ' ' + self._mesdasorte, alin='dir')))
        frag.append((dict(texto='-'.join([ut.verde(dz) if dz in self._acertadas[ns] else dz for dz in self.numeros]),
                          alin='dir'),))

        if num_acertos in self.__class__.pontuacao:
            i = self.__class__.pontuacao.index(num_acertos)
            frag.append((ut.verde(ct.MSG_APOSTA_PREMIADA.format(str_sort, self._sorteio.premiacao[i])),))
            self._sorteio.premiadas.append((self._sorteio.modal, self._sorteio.concurso, na+1, 
                                            self._sorteio.premiacao[i], self._sorteio.rateios[ns][i][1]))
        else:
            frag.append((ct.MSG_ACERTOS.format(str_sort, num_acertos),))
        if self._acertou_mesdasorte:
            frag.append((ut.verde(ct.MSG_ACERTOU_MESDASORTE.format(self._sorteio._extra[1])),))
            self._sorteio.premiadas.append((self._sorteio.modal, self._sorteio.concurso, na+1, 
                                            self._sorteio.premiacao[-1], self._sorteio.rateios[ns][-1][1]))
        [frag.append((msg,)) for msg in self._msgs]
        return frag

    def montadoc(self):
        frag = []
        for ns, _ in enumerate(self._acertadas):
            num_acertos = self._conta_acertos(ns)
            str_sort = 'Sorteio {}: '.format(ns + 1) if len(self._acertadas) > 1 else ''
            frag.append(E.tr(E.td(' - seu mês da sorte', colspan='2'), E.td(str(self._mesindex) + ' ' + self._mesdasorte)))
            frag.append(E.tr(E.td(E.ul(
                *[E.li(dz, {'class':'acerto'} if dz in self._acertadas[ns] else {'class':'normal'}, style=BLOCO) for dz
                  in self.numeros],
                {'class':'ul_apostadas ' + self._modal}), colspan='3')))

            if num_acertos in self.__class__.pontuacao:
                i = self.__class__.pontuacao.index(num_acertos)
                frag.append(E.tr({'class':'msg acerto'}, E.td(
                    ct.MSG_APOSTA_PREMIADA.format(str_sort, self._sorteio.premiacao[i]), colspan='3')))
            else:
                frag.append(E.tr({'class':'msg'}, E.td(ct.MSG_ACERTOS.format(str_sort, num_acertos), colspan='3')))
        if self._acertou_mesdasorte:
            frag.append(E.tr({'class':'msg acerto'}, E.td(ct.MSG_ACERTOU_MESDASORTE.format(self._sorteio._extra[1]), colspan='3')))
        return frag


class Lotofacil(modals.Lotofacil, Dezenas):
    pass


class Lotomania(modals.Lotomania, Dezenas):

    def montatela(self, ns, na):
        frag = []
        num_acertos = self._conta_acertos(ns)
        str_sort = 'Sorteio {}: '.format(ns + 1) if len(self._acertadas) > 1 else ''

        frag.append((dict(texto='-'.join([ut.verde(dz) if dz in self._acertadas[ns] else dz for dz in self.numeros[:25]]), alin='dir'),))
        frag.append((dict(texto='-'.join([ut.verde(dz) if dz in self._acertadas[ns] else dz for dz in self.numeros[25:]]), alin='dir'),))

        if num_acertos in self.__class__.pontuacao:
            i = self.__class__.pontuacao.index(num_acertos)
            frag.append((ut.verde(ct.MSG_APOSTA_PREMIADA.format(str_sort,
                                                                                           self._sorteio.premiacao[
                                                                                               i])),))
            self._sorteio.premiadas.append((self._sorteio.modal, self._sorteio.concurso, na+1, 
                                            self._sorteio.premiacao[i], self._sorteio.rateios[ns][i][1]))
        else:
            frag.append((ct.MSG_ACERTOS.format(str_sort, num_acertos),))

        [frag.append((msg,)) for msg in self._msgs]
        return frag

    def montadoc(self):
        frag = []
        for ns, _ in enumerate(self._acertadas):
            num_acertos = self._conta_acertos(ns)
            str_sort = 'Sorteio {}: '.format(ns + 1) if len(self._acertadas) > 1 else ''

            frag.append(
            E.tr(E.td(E.ul(
                *[E.li(dz, {'class':'acerto'} if dz in self._acertadas[ns] else {'class':'normal'}, style=BLOCO) for dz
                  in self.numeros[:25]], {'class':'ul_apostadas ' + self._modal}), colspan='3')))
            frag.append(
            E.tr(E.td(E.ul(
                *[E.li(dz, {'class':'acerto'} if dz in self._acertadas[ns] else {'class':'normal'}, style=BLOCO) for dz
                  in self.numeros[25:]], {'class':'ul_apostadas ' + self._modal}), colspan='3'))
            )

            if num_acertos in self.__class__.pontuacao:
                i = self.__class__.pontuacao.index(num_acertos)
                frag.append(E.tr({'class':'msg acerto'}, E.td(
                    ct.MSG_APOSTA_PREMIADA.format(str_sort, self._sorteio.premiacao[i]), colspan='3')))
            else:
                frag.append(
                    E.tr({'class':'msg'}, E.td(ct.MSG_ACERTOS.format(str_sort, num_acertos), colspan='3')))

        return frag


class Quina(modals.Quina, Dezenas):
    pass


class Timemania(modals.Timemania, Dezenas):
        
    def _prevalidar(self):
        # no arquivo de apostas o time do coração é sempre a ultima dezena informada
        # na informação manual é dado no parametro "-t"
        #
        super()._fatiar()
        self._obt_timecoracao(self._dvargs.get('t') or self._lst_dezenas.pop())
        super()._prevalidar()

    def conferir(self):
        # for sorteadas in self._sorteio.sorteadas:
            # self._acertadas.append(list(set(self.numeros) & set(sorteadas)))
        super().conferir()
        self._acertou_timecoracao = self._timecoracao == self._sorteio._extra[1]

    def montatela(self, ns, na):
        frag = []
        num_acertos = self._conta_acertos(ns)
        str_sort = 'Sorteio {}: '.format(ns + 1) if len(self._acertadas) > 1 else ''

        frag.append((' - seu time do coração', dict(texto=str(self._timeindex) + ' ' + self._timecoracao, alin='dir')))
        frag.append((dict(texto='-'.join([ut.verde(dz) if dz in self._acertadas[ns] else dz for dz in self.numeros]),
                          alin='dir'),))

        if num_acertos in self.__class__.pontuacao:
            i = self.__class__.pontuacao.index(num_acertos)
            frag.append((ut.verde(ct.MSG_APOSTA_PREMIADA.format(str_sort, self._sorteio.premiacao[i])),))
            self._sorteio.premiadas.append((self._sorteio.modal, self._sorteio.concurso, na+1, 
                                            self._sorteio.premiacao[i], self._sorteio.rateios[ns][i][1]))
        else:
            frag.append((ct.MSG_ACERTOS.format(str_sort, num_acertos),))
        if self._acertou_timecoracao:
            frag.append((ut.verde(ct.MSG_ACERTOU_TIMEDOCORACAO.format(self._sorteio._extra[1])),))
            self._sorteio.premiadas.append((self._sorteio.modal, self._sorteio.concurso, na+1,
                                            self._sorteio.premiacao[-1], self._sorteio.rateios[ns][-1][1]))

        [frag.append((msg,)) for msg in self._msgs]
        return frag

    def montadoc(self):
        frag = []
        for ns, _ in enumerate(self._acertadas):
            num_acertos = self._conta_acertos(ns)
            str_sort = 'Sorteio {}: '.format(ns + 1) if len(self._acertadas) > 1 else ''
            frag.append(E.tr(E.td(' - seu time do coração', colspan='2'), E.td(str(self._timeindex) + ' ' + self._timecoracao)))
            frag.append(E.tr(E.td(E.ul(
                *[E.li(dz, {'class':'acerto'} if dz in self._acertadas[ns] else {'class':'normal'}, style=BLOCO) for dz
                  in self.numeros],
                {'class':'ul_apostadas ' + self._modal}), colspan='3')))

            if num_acertos in self.__class__.pontuacao:
                i = self.__class__.pontuacao.index(num_acertos)
                frag.append(E.tr({'class':'msg acerto'}, E.td(
                    ct.MSG_APOSTA_PREMIADA.format(str_sort, self._sorteio.premiacao[i]), colspan='3')))
            else:
                frag.append(E.tr({'class':'msg'}, E.td(ct.MSG_ACERTOS.format(str_sort, num_acertos), colspan='3')))
        if self._acertou_timecoracao:
            frag.append(E.tr({'class':'msg acerto'}, E.td(ct.MSG_ACERTOU_TIMEDOCORACAO.format(self._sorteio._extra[1]), colspan='3')))

        return frag


class Federal(modals.Federal, Dezenas):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._bil = apifederal.Bilhete(('').join(self.numeros), *self._sorteio.sorteadas)

    def _confere_display(self): 
        if len(self._lst_prevalidas) > self.__class__.max:
            self._msgs.append(' - núm. do bilhete inválido ({}), verifique! (min: {}, máx: {})'.format(len(self._lst_prevalidas), self.__class__.min, self.__class__.max))
        elif len(self._lst_prevalidas) < self.__class__.max:
            while len(self._lst_prevalidas) < 5:
                self._lst_prevalidas.insert(0, '0')

    def conferir(self):
        """Apenas recebe os acertos da `apifederal`."""
        
        self._acertadas.append(self._bil.acerto)

    def _conta_acertos(self, ns):
        if self._bil.premiado:
            return len([x for x, y in zip(self.numeros, self._sorteio.sorteadas[ns][self._bil.ordem]) if x == y])
        else:
            return 0

    def montatela(self, ns, na):
        frag = []
        num_acertos = self._conta_acertos(ns)

        frag.append((dict(texto=''.join([ut.verde(x) if x == y and self._bil.premiado else x for x, y in zip(self.numeros, [dg for dg in self._sorteio.sorteadas[ns][self._bil.ordem]])]), alin='dir'),))

        if self._bil.premiado:
            frag.append((ut.verde(self._bil.msg + ' ({} acerto(s))'.format(num_acertos)),))
            self._sorteio.premiadas.append((self._sorteio.modal, self._sorteio.concurso, na+1, 
                                            'prêmio', 'consulte prêmio'))
        else:
            frag.append((' - {} acerto(s)'.format(num_acertos),))

        [frag.append(msg) for msg in self._msgs]
        return frag

    def montadoc(self):
        frag = []
        for ns, _ in enumerate(self._acertadas):
            # self._prepara_html(ns)

            num_acertos = self._conta_acertos(ns)
            
            frag.append(E.tr(E.td(E.ul(*[E.li(x, {'class':'acerto'} if x == y and self._bil.premiado else {'class':'normal'}, style=BLOCO) for x, y in zip(self.numeros, [dg for dg in self._sorteio.sorteadas[ns][self._bil.ordem]])],
                                       {'class':'ul_apostadas ' + self._modal}), colspan='3')))

            if self._bil.premiado:
                frag.append(E.tr({'class':'msg acerto'}, E.td(self._bil.msg + ' ({} acerto(s))'.format(num_acertos), colspan='3')))
            else:
                frag.append(E.tr({'class':'msg'}, E.td(' - {} acerto(s)'.format(num_acertos), colspan='3')))

        return frag
        

objs = [Megasena, Duplasena, Diadesorte , Lotofacil, Lotomania, Quina, Timemania, Lotogol, Loteca, Federal]

