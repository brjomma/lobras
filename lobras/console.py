﻿#!/usr/bin/python
# coding: utf-8
"""Um `framework` simples para gerenciar a formatação na tela."""

__version__ = '0.1.0'

__copyright__ = 'Copyright 2018, @brjomma <brjomma@gmail.com>'
__license__ = 'MIT <https://mit-license.org/>'


import re


class Console(object):
    """Produz uma lista de impressão formatada na tela com a largura desejada.
    
    Permite definir a largura desejada do *framework* e automaticamente redimensiona 
    as colunas. Também produz facilmente linhas subsequentes com larguras
    de colunas diferentes. Por exemplo: uma linha com três colunas seguida de outra 
    linha de cinco colunas, e etc...
    
    :param int larg: A largura do *framework*, em número de caracteres.
        
    """
    
    def __init__(self, larg=74):
        self._larg = larg
        self._colunas = []
        self._dados = []
        self._sep = '|'

    def addlin(self, *cols, showsep=False):
        self.showsep = showsep
        self._colunas = []
        for col in cols:
            if isinstance(col, dict):
                self._colunas.append(Col(**col))
            elif isinstance(col, tuple):
                self._colunas.append(Col(*col))
            elif isinstance(col, str):
                self._colunas.append(Col(col))
        self._calcular()
        self._normalizar()
        self._gravalinha()
            
    def addlist(self, lista, showsep=False):
        """Incorpora um fragmento em forma de lista, cada linha é uma tupla.
        
        :param list lista: Uma lista *container* do fragmento de impressão.
        :param bool showsep: Exibe ou não o separador de colunas. O padrão é *não exibir*.
        
        Aceita parâmetros empacotados para as colunas em forma de :class:`str`, 
        :class:`tuple` e :class:`dict`.
        
        """
        
        for lin in lista:
            self.addlin(*lin, showsep)
        
    def _normalizar(self):
        for col in self._colunas:
            col.atualizar()
            
    def _gravalinha(self):
        linha = ''
        for nc, col in enumerate(self._colunas):
            linha += (self._sep if nc != 0 and self.showsep else '') + col.texto
        if linha != '':
            self._dados.append(linha)

    def addbar(self, char=''):
        """Adiciona uma linha horizontal no *framework*.
        
        :param str char: Qualquer *string* válido no Python.
        
        Se o parâmetro for informado, preencherá toda a largura. 
        Se estiver ausente será adicionada uma linha em branco.
        """
        
        self._dados.append(char * self._larg)    
        
    def extendlist(self, lista):
        """Agrega uma lista (fragmento) ao conteúdo dos dados.
        
        O fragmento é adicionado no fim da lista de dados.
        """
        
        self._dados.extend(lista)
        
    def _calcular(self):
        if len(self._colunas):
            if self.showsep:
                qsep = len(self._colunas) - 1
            else:
                qsep = 0
            soma_fixas = sum([col.largfixa for col in self._colunas if col.largfixa])

            larg_resto = self._larg - soma_fixas - qsep
            n_calculadas = len([col for col in self._colunas if not col.largfixa])
            if n_calculadas:
                larg_calc = larg_resto // n_calculadas
                gap = larg_resto % n_calculadas
            else:
                larg_calc = larg_resto // len(self._colunas)
                gap = larg_resto % len(self._colunas)

            for col in self._colunas:
                if not col.largfixa:
                    col.define_larg(larg_calc)
            self._colunas[-1].define_larg(larg_calc + gap)
    
    def imprimir(self):
        """Imprime o conteúdo dos dados na tela."""
        
        for linha in self._dados:
            print(linha)    

            
class Col(object):
    """Configura uma coluna do *framework*.

    :param str texto: o texto para a coluna.
    :param int larg: A largura da coluna, em número de caracteres.
    :param str alin: O alinhamento da colunas (*esq*, *cen*, *dir*).
    :param str cor: A definição de cor para o texto completo. Depende de `colorama <https://pypi.org/project/colorama/>`_.  **Ainda não implementado**.
    """
    
    def __init__(self, texto=None, larg=15, alin='esq', cor=None):
        self.texto = texto
        self._alin = alin
        self.largcalc = larg
        self.largfixa = larg if larg != 15 else None
        self._larg = self.largfixa or self.largcalc
        self.cor = cor
        
    def atualizar(self):
        self._larg = self.largfixa or self.largcalc
        re_nostring = re.compile(r'\x1b[^m]*m')
        texto_limpo = re_nostring.sub('', self.texto)
        diff = len(self.texto) - len(texto_limpo)
        
        if self._alin == 'dir':
            self.texto = self.texto.rjust(self._larg + diff)
        elif self._alin == 'cen':
            self.texto = self.texto.center(self._larg + diff)
        else:
            self.texto = self.texto.ljust(self._larg + diff)

    def define_larg(self, larg):
        """Define a largura da coluna.
        
        Esta função é utilizada na configuração e recálculo das colunas
        antes de uma linha completa ser adicionada.
        
        :param int larg: A largura da coluna, em número de caracteres.
        
        """
        
        self.largcalc = larg
        


