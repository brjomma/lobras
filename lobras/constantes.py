﻿#!/usr/bin/python
# coding: utf-8

__version__ = '0.1.0'

__copyright__ = 'Copyright 2018, @brjomma <brjomma@gmail.com>'
__license__ = 'MIT <https://mit-license.org/>'


import os
import time


AGORA_NUM = int(time.time())
DATA_HORA = time.strftime('%d/%m/%Y-%H:%M:%S')
INFO_MARCACOES = '{} marcações'
LANDING = 'http://loterias.caixa.gov.br/wps/portal/loterias/landing/{}/'
META_NAME = 'Loterias brasileiras'
META_RESULTADO = 'Lobras, resultados'
MEUSDADOS = os.path.join(os.getcwd(), 'dados')

MSG_ACERTOS = ' - {}{} acerto(s)'
MSG_ACERTOU_MESDASORTE = ' - acertou o mês da sorte: {}'
MSG_ACERTOU_TIMEDOCORACAO = ' - acertou o time do coração: {}'
MSG_APOSTA_CONFERIDA = ' - {} aposta(s) conferida(s)'
MSG_APOSTA_GANHOU = ' - {} {}: aposta {}, ganhou {}: {}'
MSG_APOSTA_PREMIADA = ' - {}Parabéns! aposta premiada ({})'
MSG_APOSTA_SIMULADA = ' {} aposta(s) simulada(s)'
MSG_ARQ_NAO_ENCONTRADO = ' - impossível abrir ({}), verifique!'
MSG_BUSC_CONCURSO =  'Buscando Concursos...'
MSG_CONCURSO_APOSTA_PREMIO = ' - {} concurso(s), {} aposta(s), {} prêmio(s)'
MSG_CONF_FORCADA = ' - atenção, concurso vencido (conferência forçada)'
MSG_CONF_NAO_RECOMENDADA = ' - concurso vencido (conferência não recomendada), -V confere'
MSG_CRIANDO_RESUMO = 'Criando Resumo.......'
MSG_DEZ_DUPLICADA_VER = ' - dezena(s) duplicada(s) ({}), verifique!'
MSG_DEZ_INVALIDA_VER = ' - dezena(s) inválida(s) ({}), verifique! (1 a {})'
MSG_MARC_FIXAS = ' - ({}) marcação(ões) fixa(s) [{}]'
MSG_MARC_INVALIDA_VER = ' - marcação(ões) inválida(s) ({}), verifique!'
MSG_MARC_MIN_MAX = ' - núm. de marcações inválido ({}), verifique! (min: {}, máx: {})'
MSG_NUM_DEZ_INVALIDO_VER = ' - núm. de dezenas inválido ({}), verifique! (min: {}, máx: {})'
MSG_OBTIDO_CACHE = ' - resultado obtido do cache'
MSG_OBTIDO_INTERNET = ' - resultado obtido da internet'
MSG_SEM_APOSTAS = ' - sem a postas para conferir'
MSG_SEM_APOSTA_PEND = ' - não existem apostas pendentes'
MSG_VALID_CONCURSO = ' - validade do concurso: {} {}'

TIT_PENDENTES = 'Concursos e apostas pendentes'
TIT_RESULTADOS = 'Lobras, Resultado da '
TIT_RESULTADO_SORTEIO = 'Resultado do sorteio'
TIT_RESUMO = 'Lobras, Resumo dos prêmios principais'
TIT_RODAPE = 'Lobras, Resumo da conferência'
TIT_SIMULACOES = 'Lobras, Simulações da '