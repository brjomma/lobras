﻿#!/usr/bin/python
# coding: utf-8
"""Um construtor de documentos `html`."""

__version__ = '0.1.0'

__copyright__ = 'Copyright 2018, @brjomma <brjomma@gmail.com>'
__license__ = 'MIT <https://mit-license.org/>'


import os
import webbrowser
from lxml import etree
from lxml.builder import E #elementMaker
import constantes as ct


class Doc(object):
    """formatação de objeto `lxml <https://lxml.de/>`_ para gerar um arquivo *html*."""
    
    def __init__(self):
        self.html = E.html({'lang':'pt-br'})
        self.head = E.head()
        self.body = E.body()
        self.header = E.header()
        self.section = E.section()
        self.footer = E.footer()
        self._filename = None

    def salvar(self, filename):
        """Grava o objeto `lxml <https://lxml.de/>`_ em um arquivo."""
        
        self.body.append(self.header)
        self.body.append(self.section)
        self.body.append(self.footer)
        self.html.append(self.head)
        self.html.append(self.body)
        self._doc = etree.ElementTree(self.html)        
        
        self._filename = filename
        self._doc.write(os.path.join(ct.MEUSDADOS, self._filename), encoding='UTF-8', pretty_print=True, doctype='<!DOCTYPE html>')
        
    def browser(self):
        """Exibe o arquivo *html* no browser padrão."""
        
        try:
            webbrowser.open_new_tab(os.path.join(ct.MEUSDADOS, self._filename))
        except:
            print(' - nenhum arquivo html para este resultado!')
        