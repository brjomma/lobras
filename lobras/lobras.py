﻿#!/usr/bin/python
# coding: utf-8

__version__ = '0.1.0'

__copyright__ = 'Copyright 2018, @brjomma <brjomma@gmail.com>'
__license__ = 'MIT <https://mit-license.org/>'


import os
import sys
import argparse
import textwrap
from utils import sobre
from simulacoes import Simulacao
from resultados import Avulso, Pacote
from resumo import Resumo
import constantes as ct


def verifica_local_dados():
    if not os.path.exists(ct.MEUSDADOS):
        os.makedirs(ct.MEUSDADOS)

def informar(dvargs):
    print('='*80)
    print('Sobre a {}'.format(dvargs['modal']))
    print('='*80)
    print(textwrap.fill(sobre['SOBRE_' + dvargs['modal'].upper()], width=80))
    
def simular(dvargs):
    s = Simulacao(dvargs)
    s.imprimir()
    
def conferir(dvargs):
    if dvargs.get('f'):
        r = Pacote(dvargs)
    else:
        r = Avulso(dvargs)
    r.imprimir()

    if dvargs.get('H'):
        if not dvargs.get('u'):
            # Doc html único
            r.browser()
        else:
            # vários Doc html
            r.browse_sorteios()

def resumir(dvargs):
    resumo = Resumo(dvargs)
    if dvargs.get('r'):
        resumo.imprime_console()
        resumo.imprime_sorteios()
        resumo.injeta_sorteios()
    
    elif dvargs.get('f'):
        if dvargs.get('n'):
            resumo.add_pacote()
            resumo.imprime_console()
            resumo.imprime_pacotes()
            resumo.injeta_pacotes()
        else:
            resumo.imprime_console()
    else:
        resumo.imprime_console()
        
    if dvargs.get('H'):
        resumo.browser()

def parametrizar():
    """
    Analisador de argumentos.
    """
    parser = argparse.ArgumentParser(prefix_chars='-/', prog='br', usage='br [--modal] comando [-opções] [args]')
    gr_modal = parser.add_mutually_exclusive_group()
    gr_modal.add_argument('--megasena', '//megasena',
                action='store_const', 
                dest='modal', 
                const='megasena', 
                default='megasena', 
                help='modalidade numérica')
    gr_modal.add_argument('--duplasena', '//duplasena', 
                action='store_const', 
                dest='modal',  
                const='duplasena',  
                help='modalidade numérica')
    gr_modal.add_argument('--diadesorte', '//diadesorte', 
                action='store_const', 
                dest='modal',  
                const='diadesorte',  
                help='modalidade numérica')
    gr_modal.add_argument('--lotofacil', '//lotofacil', 
                action='store_const', 
                dest='modal',  
                const='lotofacil',  
                help='modalidade numérica')
    gr_modal.add_argument('--lotomania', '//lotomania', 
                action='store_const', 
                dest='modal',  
                const='lotomania',  
                help='modalidade numérica')
    gr_modal.add_argument('--quina', '//quina',
                action='store_const', 
                dest='modal', 
                const='quina',  
                help='modalidade numérica')
    gr_modal.add_argument('--timemania', '//timemania',
                action='store_const', 
                dest='modal',  
                const='timemania',  
                help='modalidade numérica')
    gr_modal.add_argument('--lotogol', '//lotogol',  
                action='store_const', 
                dest='modal',  
                const='lotogol',    
                help='modalidade esportiva')
    gr_modal.add_argument('--loteca', '//loteca',    
                action='store_const', 
                dest='modal',  
                const='loteca',     
                help='modalidade esportiva')
    gr_modal.add_argument('--federal', '//federal',    
                action='store_const', 
                dest='modal',  
                const='federal',     
                help='modalidade federal')
    gr_modal.set_defaults(func=informar)

    subparser = parser.add_subparsers(title='comandos disponíveis')

    parser_simula = subparser.add_parser('simula', aliases=['s'], prefix_chars='-/', help='simulação de apostas', usage='br [--modal] simula [-opções] [args]')
    parser_simula.add_argument('-d', '/d', 
                    metavar='D',
                    action='store', 
                    default=[None],                 
                    nargs='+', 
                    dest='lst_dezenas', 
                    help='Entidade dezenas. Encadear com traço e separar por espaço. Ex: -d 7-8-19-20-21-22 15-22-30')
    parser_simula.add_argument('-n', '/n',
                    metavar='N',
                    action='store', 
                    default=[None],                 
                    nargs='+', 
                    dest='lst_ndez',  
                    type=int, 
                    help='Entidade núm. dezenas, separar por espaço. Ex: -n 12 7')
    parser_simula.add_argument('-q', '/q',
                    metavar='',
                    action='store', 
                    default=1,                 
                    type=int, 
                    help='Quantidade de apostas para simular')
    parser_simula.add_argument('-t', '/t',
                    metavar='',
                    action='store', 
                    default=argparse.SUPPRESS,
                    type=int, 
                    help='Código do time do coração ou do mês da sorte')
    parser_simula.add_argument('-H', '/H', 
                    action='store_true', 
                    default=argparse.SUPPRESS,
                    help='Salva um arquivo e exibe o html')
    parser_simula.add_argument('-D', '/D',
                    action='store', 
                    default=argparse.SUPPRESS,
                    type=int,
                    help='Quantidade de duplos na loteca')
    parser_simula.add_argument('-T', '/T',
                    action='store', 
                    default=argparse.SUPPRESS,
                    type=int,
                    help='Quantidade de triplos na loteca')
    parser_simula.set_defaults(func=simular)

    parser_result = subparser.add_parser('result', aliases=['r'], prefix_chars='-/', help='resultado e conferência', usage='br [--modal] result [-opções] [args]')
    gr_file = parser_result.add_mutually_exclusive_group() # ou dezenas avulsas ou arquivo csv
    gr_file.add_argument('-d', '/d', 
                    metavar='D',
                    action='store', 
                    default=[None],                 
                    nargs='+', 
                    dest='lst_dezenas', 
                    help='Entidade dezenas. Encadear com traço e separar por espaço. Ex: -d 7-8-19-20-21-22 15-22-30')
    gr_file.add_argument('-f', '/f', 
                    metavar='arquivo.csv', 
                    action='store', 
                    default=argparse.SUPPRESS,
                    nargs='+', 
                    help='Analisa um arquivo e confere os resultados')
    parser_result.add_argument('-c', '/c', 
                    metavar='concursos',
                    action='store', 
                    default=[None],
                    nargs='+', 
                    dest='lst_concursos', 
                    help='Entidade concursos, separadas por espaço. Ex: -c 17 100 1000-1010')
    parser_result.add_argument('-i', '/i', 
                    action='store_true', 
                    default=argparse.SUPPRESS,
                    help='Força a consulta na Internet')
    parser_result.add_argument('-t', '/t',
                    metavar='',
                    action='store', 
                    default=argparse.SUPPRESS,                 
                    type=int, 
                    help='Código do time do coração ou do mês da sorte')
    parser_result.add_argument('-H', '/H', 
                    action='store_true', 
                    default=argparse.SUPPRESS,
                    help='Salva um arquivo e exibe o html')
    parser_result.add_argument('-u', '/u', 
                    action='store_true', 
                    default=argparse.SUPPRESS,
                    help='Individualiza cada concurso em um arquivo html')
    parser_result.add_argument('-V', '/V', 
                    action='store_true', 
                    default=argparse.SUPPRESS,
                    help='Confere sorteios vencidos')
    parser_result.add_argument('-X', '/X', 
                    action='store_true', 
                    default=argparse.SUPPRESS,
                    help='Deleta o cache existente antes da consulta')
    parser_result.set_defaults(func=conferir)

    parser_resumo = subparser.add_parser('Resumo', aliases=['R'], prefix_chars='-/', help='resumo de todas as modalidades', usage='br Resumo [-opções] [args]')
    gr_ordenar = parser_resumo.add_mutually_exclusive_group() # ou dezenas avulsas ou arquivo csv
    gr_ordenar.add_argument('-a', '/a', 
                    action='store_true', 
                    default=argparse.SUPPRESS,
                    help='Ordena alfabeticamente')
    gr_ordenar.add_argument('-p', '/p', 
                    action='store_true', 
                    default=argparse.SUPPRESS,
                    help='Ordena pelo próximo concurso')
    gr_pacotes = parser_resumo.add_mutually_exclusive_group() # só os sorteios ou sorteios + apostas
    gr_pacotes.add_argument('-f', '/f', 
                    metavar='arquivo.csv', 
                    action='store', 
                    default=argparse.SUPPRESS,
                    nargs='+', 
                    help='Acrescenta no resumo as conferências pendentes')
    gr_pacotes.add_argument('-r', '/r', 
                    action='store_true', 
                    default=argparse.SUPPRESS,
                    help='Imprime o resultado dos sorteios após resumir')
    parser_resumo.add_argument('-n', '/n', 
                    action='store_true', 
                    default=argparse.SUPPRESS,
                    help='Imprime o resultado dos sorteios e das apostas após resumir')
    parser_resumo.add_argument('-H', '/H', 
                    action='store_true', 
                    default=argparse.SUPPRESS,
                    help='Salva um arquivo e exibe o html')
    parser_resumo.add_argument('-i', '/i', 
                    action='store_true', 
                    default=argparse.SUPPRESS,
                    help='Força a consulta na Internet')
    parser_resumo.add_argument('-V', '/V', 
                    action='store_true', 
                    default=argparse.SUPPRESS,
                    help='Confere sorteios vencidos')
    parser_resumo.set_defaults(func=resumir)
    
    modals = [i.const for i in gr_modal._actions if isinstance(i, argparse._StoreConstAction)]
    return parser, modals
    
def main():
    """Obtém um dicionário com os parâmetros de linha e as modalidades.
    
    O dicionário será verticalizado para os sorteios, as apostas e as dezenas
    através das suas respectivas funções.
    """
    
    verifica_local_dados()
    parser, modals = parametrizar()
    dvargs = vars(parser.parse_args())
    dvargs['modals'] = modals
    dvargs['func'](dvargs)
    
if __name__ == '__main__':
    main()


