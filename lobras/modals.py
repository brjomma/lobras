﻿#!/usr/bin/python
# coding: utf-8

__version__ = '0.1.0'

__copyright__ = 'Copyright 2018, @brjomma <brjomma@gmail.com>'
__license__ = 'MIT <https://mit-license.org/>'


import random
import utils as ut
import constantes as ct


class Dezenas():
    
    def _fatiar(self):
        # detecta qualquer separador não numérico
        #
        sep = ut.sep(self._flatdezenas)
        self._lst_dezenas = self._flatdezenas.split(sep=sep) if self._flatdezenas else []
    
    def _prevalidar(self):
        populacao = range(1, self.__class__.display+1)

        # Extrai as dezenas estranhas
        lst_estranhas = [dz for dz in self._lst_dezenas if not dz.isdigit()]
        
        # preenche com zeros para formar duas casas decimais
        lst_validas = [dz.zfill(2) for dz in self._lst_dezenas if dz.isdigit()]
        
        # extrai as dezenas duplicadas sem excluir e emite mensagem
        lst_duplicadas = sorted(set([dz for dz in lst_validas if lst_validas.count(dz)>1]))
        if lst_duplicadas:
            self._msgs.append(ct.MSG_DEZ_DUPLICADA_VER.format('-'.join(lst_duplicadas)))
            
        # extrai as dezenas inválidas e emite mensagem
        lst_invalidas = [dz for dz in lst_validas if int(dz) not in populacao]
        if lst_invalidas or lst_estranhas:
            self._msgs.append(ct.MSG_DEZ_INVALIDA_VER.format('-'.join(lst_invalidas+lst_estranhas), self.__class__.display))
            
        # aplica filtro para excluir dezenas fora do display
        lst_revalidas = [dz for dz in lst_validas if int(dz) in populacao]
        
        # finaliza e prevalida as dezenas, ordenando e excluindo duplicatas 
        # essa lista ordenada será exibida nas dezenas do coração
        self._lst_prevalidas = sorted(set([dz for dz in lst_revalidas]), key=int)

        self.numeros = self._lst_prevalidas


class Lotogol():
    display = ['00', '01', '02', '03', '0+', '10', '11', '12', '13', '1+',
           '20', '21', '22', '23', '2+', '30', '31', '32', '33', '3+',
           '+0', '+1', '+2', '+3', '++']
    min = 5
    max = 5
    pontuacao = [5, 4, 3]

    def _fatiar(self):
        #separador obrigatório '-'
        # modalidade tem dezena não numérica. Ex: ++
        #
        self._lst_dezenas = self._flatdezenas.split('-') if self._flatdezenas else []
    
    def _prevalidar(self):
        
        lst_estranhas = [dz for dz in self._lst_dezenas if dz not in self.__class__.display]
        if lst_estranhas:
            self._msgs.append(ct.MSG_MARC_INVALIDA_VER.format('-'.join(lst_estranhas)))
        self._lst_prevalidas = [dz for dz in self._lst_dezenas if dz in self.__class__.display]
        self.numeros = self._lst_prevalidas


class Loteca(Dezenas):
    display = ['0', '1', '2']
    min = 14
    max = 14
    pontuacao = [14, 13]
    
    def _prevalidar(self):
        duplosetriplos = ['01', '02', '10', '012', '12', '20', '021', '21', '102', '120', '201', '210']
        sep = ut.sep(self._flatdezenas)
        lst_dezenas = self._flatdezenas.split(sep=sep) if self._flatdezenas else []
        lst_estranhas = [dz for dz in lst_dezenas if dz not in self.__class__.display + duplosetriplos]
        if lst_estranhas:
            self._msgs.append(ct.MSG_MARC_INVALIDA_VER.format('-'.join(lst_estranhas)))
        self._lst_prevalidas = [dz for dz in lst_dezenas if dz in self.__class__.display + duplosetriplos]
        self.numeros = self._lst_prevalidas

    def _valida_extras(self, duplos, triplos):
        """Validação dos duplos e triplos.

        Em caso de combinação não permitida, será simulada um aposta simples
        onde é permitido apenas um duplo.
        """

        tabela = [(0, 6), (1, 5), (3, 4), (5, 3), (6, 2), (8, 1), (9, 0)]
        possibilidades = []
        for col in tabela:
            r1 = range(1) if col[0] == 0 else range(1, col[0] + 1)
            r2 = range(1) if col[1] == 0 else range(1, col[1] + 1)
            possibilidades.extend([(x, y) for x in r1 for y in r2])

        possibilidades = sorted(set(possibilidades))
        dutri = (duplos, triplos)
        msg = None

        if dutri in possibilidades:
            if dutri[1] == 0:
                msg = ' - {} duplo(s)'.format(dutri[0])
            elif dutri[0] == 0:
                msg = ' - {} triplo(s)'.format(dutri[1])
            else:
                msg = ' - {} duplo(s) e {} triplo(s)'.format(*dutri)
        else:
            if dutri != (0, 0):
                msg = ' - não permitido, {} duplo(s) e {} triplo(s). verifique!'.format(*dutri)
            
        return dutri in possibilidades, msg
        

class Megasena():
    display = 60
    min = 6
    max = 15
    pontuacao = [6, 5, 4]


class Duplasena():
    display = 50
    min = 6
    max = 15
    pontuacao = [6, 5, 4, 3]


class Diadesorte():
    display = 31
    min = 7
    max = 15
    pontuacao = [7, 6, 5, 4, 99]

    def _obt_mesdasorte(self, m):
        ms = 'MÊS NÃO LOCALIZADO, tente 1 a 12'
        try:
            if m is None:
                m = random.choice(range(1, 13))
            elif not isinstance(m, int):
                m = int(''.join([d for d in m if d.isdigit()]) or '99')

            ms = ut.MS[m]
        except KeyError:
            m = 99
        finally:
            self._mesdasorte, self._mesindex = ms, m


class Lotofacil():
    display = 25
    min = 15
    max = 18
    pontuacao = [15, 14, 13, 12, 11]


class Lotomania():
    display = 100
    min = 50
    max = 50
    pontuacao = [20, 19, 18, 17, 16, 15, 00]
    
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._transformar_100()

    def _transformar_100(self):
        if '100' in self.numeros:
            i = self.numeros.index('100')
            self.numeros[i] = '00'
        self.numeros.sort(key=int)  # dezenas numéricas devem ser ordenadas
       

class Quina():
    display = 80
    min = 5
    max = 15
    pontuacao = [5, 4, 3, 2]


class Timemania():
    display = 80
    min = 10
    max = 10
    pontuacao = [7, 6, 5, 4, 3, 99]
    
    def _obt_timecoracao(self, t):
        tc = 'TIME NÃO LOCALIZADO, tente 1 a 80'
        try:
            if t is None:
                t = random.choice(range(1, self.__class__.display+1))
            elif not isinstance(t, int):
                t = int(''.join([d for d in t if d.isdigit()]) or '99')

            tc = ut.TC[t]
        except KeyError:
            t = 99
        finally:
            self._timecoracao, self._timeindex = tc, t


class Federal():
    display = 91000
    min = 5
    max = 5
    pontuacao = [1, 2, 3, 4, 5]

    def _fatiar(self):
        self._lst_dezenas = list(self._flatdezenas) if self._flatdezenas else []

    def _prevalidar(self):

        lst_estranhas = [dg for dg in self._lst_dezenas if not dg.isdigit() and not dg == '-']
        if len(lst_estranhas):
            self._msgs.append(' - dígito(s) inválido(s) ({}), verifique!'.format('-'.join(lst_estranhas)))

        self._lst_prevalidas = [dg for dg in self._lst_dezenas if dg.isdigit()]

        self.numeros = self._lst_prevalidas



