﻿#!/usr/bin/python
# coding: utf-8
"""Apresenta o resultado dos sorteios com ou sem apostas conferidas.

Possui um `conteiner`de sorteios. A análise pode ser feita através dos parâmetros de informações avulsas ou de 
um arquivo :mod:`csv` de apostas.

"""

__version__ = '0.1.0'

__copyright__ = 'Copyright 2018, @brjomma <brjomma@gmail.com>'
__license__ = 'MIT <https://mit-license.org/>'


import os
import csv
from lxml.builder import E #elementMaker
import sorteios
import utils as ut
from documento import Doc
from console import Console
import constantes as ct


class Resultado(object):
    """Um `container` para gerenciar objetos :class:`sorteio <sorteios.Sorteio>`.
    
    :param dict dvargs: Um dicionário que contém os argumentos passados e as modalidades.
    :param list lst_dezenas: (opcional) Uma lista com uma ou várias entidades dezenas.
    :param list lst_concursos: (opcional) Uma lista com uma ou várias entidades concurso ou sequencias.
    
    Quando existir um arquivo informado ``-f``, a conferência será em lote, caso contrário será avulsa ``-d``

    """
    
    def __init__(self, dvargs):
        self._dvargs = dvargs
        self.modal = self._dvargs['modal']
        self.indexmodal = self._dvargs['modals'].index(self.modal)
        self._lst_dezenas = self._dvargs.get('lst_dezenas') or [None]
        self._lst_concursos = set(ut.sequenciar(self._dvargs.get('lst_concursos') or [None]))
        
        self._sorteios = []
        self._rodape = []
        self._console = Console()
        self._doc = Doc()

    def _add_sorteio(self, csvexist=None):
        obj = sorteios.objs[self.indexmodal]
        for nconcurso in self._lst_concursos:
            sorteio = obj(self._dvargs, lst_dezenas=self._lst_dezenas, nconcurso=nconcurso)
            self._sorteios.append(sorteio)
        self._sorteios.sort(key=lambda obj: (int(obj.concurso)))
              
    def imprimir(self):
        """Descarrega o conteúdo dos dados do objeto :class:`console <console.Console>` na tela do terminal.
        
        Por padrão, a opção ``-H`` salva o :class:`documento <documento.Doc>` em um único arquivo *html* e o exibe no browser padrão.
        
        Se a opção ``-u`` for adicionada, os concursos serão salvos em um ou vários arquivos individuais.
        
        ..  note::
        
            Isso pode afetar a performance do seu navegador, dependendo do tamanho do seu arquivo.
        
        """
        
        for sorteio in self._sorteios:
            sorteio.imprimir()
        self._imprime_rodape()

    def _imprime_rodape(self):
        self._console.imprimir()

    def browser(self):
        """Salva todos os concursos em um arquivo *html* único.
        
        Depois exibe o arquivo no browser padrão.
        """
        
        self._salvar()
        self._browser()

    def _salvar(self):
        self._doc.salvar('resultados_{}.html'.format(self.modal))
        
    def _browser(self):
        self._doc.browser()
    
    def browse_sorteios(self):
        """Salva um objeto :class:`documento <documento.Doc>` em arquivos *html* individuais.
        
        Depois exibe os arquivos no browser padrão. Pode afetar a performance
        do seu navegador, dependendo do tamanho do seu arquivo.
        """
        
        self._salvar_sorteios()
        self._browser_sorteios()

    def _salvar_sorteios(self):
        for sorteio in self._sorteios:
            sorteio.salvar()
            
    def _browser_sorteios(self):
        for sorteio in self._sorteios:
            sorteio.browser()

    def _monta_rodape(self):
        if len(self._sorteios):
            soma_sorteio = len(self._sorteios)
            soma_aposta = 0
            soma_premiada = 0
            temp = []
        
            for sorteio in self._sorteios:
                soma_aposta += len(sorteio._apostas)
                soma_premiada += len(sorteio.premiadas)
                for premiada in sorteio.premiadas:
                    temp.append(ct.MSG_APOSTA_GANHOU.format(*premiada))
            self._rodape.append(ct.MSG_CONCURSO_APOSTA_PREMIO.format(soma_sorteio, soma_aposta, soma_premiada))
            [self._rodape.append(msg) for msg in temp]
            
            # if len(self._sorteios) > 1:
            self._addtela()
            self._adddoc()
    
    def _addtela(self):
        self._console.addbar('=')
        self._console.addlin(ct.TIT_RODAPE)
        self._console.addbar('=')
        self._console.addbar()
        self._console.addlin(self._rodape)
        [self._console.addlin(msg) for msg in self._rodape]
        [self._console.addbar() for _ in range(2)]
        
    def _adddoc(self):
        self._doc.head = E.head(
                            E.title(ct.META_RESULTADO),
                            E.link(rel='stylesheet', href='../css/lobras.css', type='text/css'),
                            E.meta(name=ct.META_NAME))
        self._doc.header = E.header(E.h1(ct.TIT_RODAPE,
                                    E.span(ct.DATA_HORA)))
        self._doc.section.append(E.div({'class':'msg'}, *[E.div(msg) for msg in self._rodape]))
        for sorteio in self._sorteios:
            divsorteios = E.div({'class':'sorteios'})
            divsorteios.append(sorteio.doc.header) # body
            divsorteios.append(sorteio.doc.section)
            divsorteios.append(sorteio.doc.footer)
            self._doc.section.append(divsorteios)

            
class Avulso(Resultado):
    """Todas as informações vem do `prompt de comando`."""
    
    def __init__(self, dvargs):
        super().__init__(dvargs)
        self._add_sorteio()
        self._monta_rodape()
        
        
class Pacote(Resultado):
    """Os dados são obtidos em um ou mais arquivos csv."""
    
    def __init__(self, dvargs):
        super().__init__(dvargs)
        self._dados = self._extrair_dados()
        self._add_pacote()
        self._monta_rodape()
        
    def _add_pacote(self):
        for n, d in enumerate(self._dados):
            self._lst_dezenas, self._lst_concursos = d[0], d[1]
            self._add_sorteio()
            
            if not self._dvargs.get('n'):
                ut.progress_bar(len(self._dados), n + 1, ct.MSG_BUSC_CONCURSO)
        if not self._dvargs.get('n'):
            print()

    def _extrair_dados(self):
        dados =[]
        try:
            csvdados = self._dvargs['csvdados']
            self._dvargs['csverros'] = []
        except:
            csvdados, self._dvargs['csverros'] = ut.lerCSV(self._dvargs.get('f'))

        # ou os informados (-c xxxx) ou todos no csv (lin[1)
        # set() = excluir concursos repetidos
        #
        sequenciados = set(ut.sequenciar([c for c in self._lst_concursos if c] or [lin[1] for lin in csvdados if lin[0].lower() == self.modal]))
        if len(sequenciados):
            for nconc in sequenciados:
                lstdzs = []
                for lin in csvdados:
                    # lin[1] deve ser sequenciada como uma lista = [lin[1]]
                    # isso possibilita analisar as sequencias de concursos
                    #
                    if nconc in ut.sequenciar([lin[1]]):
                        # as dezenas da loteca não devem ser de duas casas
                        # quando extrair dados do pacote
                        #
                        if self.modal == 'loteca':
                            lstdzs.append('-'.join([dz for dz in lin[2:] if dz]))
                        # o número da federal deve ter 5 casas, para evitar mensagem de erro
                        #
                        elif self.modal == 'federal':
                            lstdzs.append('-'.join([dz.zfill(5) for dz in lin[2:] if dz]))
                        else:
                            lstdzs.append('-'.join([dz.zfill(2) for dz in lin[2:] if dz]))
                # lista de dezenas e lista de concursos [nconc]
                #
                dados.append((lstdzs, [nconc]))
        else:
            dados.append(([None], [None]))
            
        return dados

