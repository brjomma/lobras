﻿#!/usr/bin/python
# coding: utf-8
"""Apresenta um resumo gerencial de todas as modalidades.

As informações são úteis para gerenciar o volume e a destinação das futuras apostas..

"""

__version__ = '0.1.0'

__copyright__ = 'Copyright 2018, @brjomma <brjomma@gmail.com>'
__license__ = 'MIT <https://mit-license.org/>'


import os
import csv
import shelve
from lxml.builder import E #elementMaker
import sorteios
from resultados import Pacote
import utils as ut
from documento import Doc
from console import Console
import constantes as ct


class Resumo(object):
    """Um `container` para gerenciar objetos :class:`resultado <resultados.Resultado>`.
    
    :param dict dvargs: Um dicionário que contém os argumentos passados e as modalidades.
    
    """
    
    def __init__(self, dvargs):
        self._dvargs = dvargs
        self._sorteios = []
        self._pacotes = []
        self._msgs = []
        self._ultimos = self._obt_cache()
        self._resumo = []
        self._pendentes = {}
        self._console = Console()
        self._doc = Doc()

        self._abrirCSV()
        if self._ultimos == {}:
            self._add_sorteio()
        else:
            self._add_cache()

        self._montaresumo()
        if self._dvargs.get('f'):
            self._obt_pendentes()

    def _abrirCSV(self):
        self._dvargs['csvdados'], csverros = ut.lerCSV(self._dvargs.get('f'))
        for filename in csverros:
            self._msgs.append(ct.MSG_ARQ_NAO_ENCONTRADO.format(filename))

    def _add_cache(self):
        for n, modal in enumerate(self._dvargs['modals']):
            obj = sorteios.objs[n]
            self._dvargs['modal'] = modal
            sorteio = obj(self._dvargs, nconcurso=self._ultimos[modal])
            self._sorteios.append(sorteio)
            ut.progress_bar(len(self._dvargs['modals']), n + 1, ct.MSG_CRIANDO_RESUMO)
        print()

    def _add_sorteio(self):
        for n, modal in enumerate(self._dvargs['modals']):
            obj = sorteios.objs[n]
            self._dvargs['modal'] = modal
            sorteio = obj(self._dvargs)
            self._sorteios.append(sorteio)
            ut.progress_bar(len(self._dvargs['modals']), n + 1, ct.MSG_CRIANDO_RESUMO)

            self._ultimos[modal] = sorteio.concurso
        self._grv_cache()
        print()

    def add_pacote(self):
        for n, modal in enumerate(self._dvargs['modals']):
            ut.progress_bar(len(self._dvargs['modals']), n + 1, ct.MSG_BUSC_CONCURSO)
            self._dvargs['modal'] = modal
            pacote = Pacote(self._dvargs)
            self._pacotes.append(pacote)
        print()

    def _montaresumo(self):
        import datetime
        for modal, sorteio in zip(self._dvargs['modals'], self._sorteios):
            montante = sorteio.acumulado if sorteio.acumulado != 'R$ 0,00' else sorteio.rateios[0][0][1]
            prox_sorteio = ut.dia_abrev(sorteio.proximo) + ' ' + sorteio.proximo
            prox_data = datetime.datetime.strptime(sorteio.proximo, "%d/%m/%Y")
            temp = []
            temp.append(modal)
            temp.append(sorteio.concurso)
            temp.append(sorteio.rateios[0][0][0]) #ganhad
            temp.append(montante)
            temp.append(prox_sorteio)
            temp.append(float(montante.replace('R$','').replace('.','').replace(',','.')))  # float para ordenação
            temp.append(prox_data)
            self._resumo.append(temp)

        self._msgs.append(' - a federal apresenta o número do bilhete do 1\u00BA prêmio')
        self._msgs.append(' - valores em azul representam os prêmios pagos')

        if self._dvargs.get('a'):
            self._resumo.sort(key=lambda i: i[0])
            self._msgs.append(' - ordenado alfabeticamente')
        elif self._dvargs.get('p'):
            self._resumo.sort(key=lambda i: i[6])
            self._msgs.append(' - ordenado por data próx.sorteio')
        else:
            self._resumo.sort(key=lambda i:i[5], reverse=True)
            self._msgs.append(' - ordem decrescente de montante')
    
    def imprime_console(self):
        self._montatela()
        self._montadoc()
        self._monta_pendentes()
        self._console.imprimir()

    def _montatela(self):
        self._console.addbar('=')
        self._console.addlin(ct.TIT_RESUMO,
                         {'texto':ct.DATA_HORA, 'alin':'dir'})
        self._console.addbar('=')
        self._console.addlin(('Modalidade', 14),
                          {'texto':'Concurso', 'larg':8, 'alin':'cen'},
                          {'texto':'Ganhadores', 'larg':11, 'alin':'cen'},
                          {'texto':'Pago/Acumulado', 'alin':'dir'},
                          {'texto':'Próximo Sorteio', 'larg':18, 'alin':'dir'}
                          )
        self._console.addbar('-')

        for linha in self._resumo:
            self._console.addlin((linha[0], 14),
                              {'texto':linha[1], 'larg':8, 'alin':'cen'},
                              {'texto':linha[2], 'larg':11, 'alin':'cen'},
                              {'texto':linha[3] if linha[2] == '0' else ut.azul(linha[3]), 'alin':'dir'},
                              {'texto':linha[4], 'larg':18, 'alin':'dir'}
                              )
        self._console.addbar('-')
        self._monta_rodape()
        [self._console.addlin(msg) for msg in self._msgs]
        [self._console.addbar() for _ in range(2)]

    def _monta_rodape(self):
        if len(self._pacotes):
            soma_sorteio = 0
            soma_aposta = 0
            soma_premiada = 0
            temp = []
        
            for pacote in self._pacotes:
                soma_sorteio += len(pacote._sorteios)
                for sorteio in pacote._sorteios:
                    soma_aposta += len(sorteio._apostas)
                    soma_premiada += len(sorteio.premiadas)
                    for premiada in sorteio.premiadas:
                        temp.append(ct.MSG_APOSTA_GANHOU.format(*premiada))
            self._msgs.append(ct.MSG_CONCURSO_APOSTA_PREMIO.format(soma_sorteio, soma_aposta, soma_premiada))
            [self._msgs.append(msg) for msg in temp]
    
    def _montadoc(self):
        self._doc.head = E.head(
                            E.title('Lobras, resumo'),
                            E.link(rel='stylesheet', href='../css/lobras.css', type='text/css'),
                            E.meta(name=ct.META_NAME))
        self._doc.header = E.header(E.h1(ct.TIT_RESUMO,
                                    E.span(ct.DATA_HORA))
                                )
        self._doc.section.append(E.div({'class':'resumo'},
                        E.table(
                            E.thead(E.tr(
                                    E.th('Modalidade'), E.th('Concurso'), E.th('Ganhadores'), E.th('Pago/Acumulado'), E.th('Próximo Sorteio'))
                                    ),
                            E.tbody(
                                *[E.tr(
                                    E.td(linha[0]), E.td(linha[1]), E.td(linha[2]), E.td(linha[3], {'class':'premiopago'} if linha[2] != '0' else {}), E.td(linha[4])
                                    ) for linha in self._resumo])
                                    )
                                    ))
        self._doc.section.append(E.div( {'class':'msg'}, 
                                        *[E.div(msg) for msg in self._msgs]))

    def _monta_pendentes(self):
        if self._dvargs.get('f'):
            self._console.addbar('=')
            self._console.addlin(ct.TIT_PENDENTES)
            self._console.addbar('=')

            if len(self._pendentes):
                [(self._console.addlin(ut.azul(k) + ' ' + str(sum([i[1] for i in v]))),
                  self._console.addlin(('', 20),
                                    {'texto':'concurso', 'alin':'cen'},
                                    {'texto':'qt.apostas', 'alin':'cen'}),
                  [self._console.addlin(('', 20),
                                     {'texto':col[0], 'alin':'cen'},
                                     {'texto':str(col[1]), 'alin':'cen'})
                   for col in v],
                  self._console.addbar('-')) for k, v in self._pendentes.items()]
                # [self._console.addbar() for _ in range(2)]

                # doc
                self._doc.section.append(E.div({'class':'pendentes'}, E.h1(ct.TIT_PENDENTES),
                                               *[E.table(E.thead(E.tr(
                                                   E.th(E.span(k, {'class':'modal'}), E.span(str(sum([i[1] for i in v]))),
                                                        colspan='2')), E.tr(E.th('concurso'), E.th('qt.apostas'))),
                                                         E.tbody(*[E.tr(E.td(col[0]), E.td(str(col[1]))) for col in v])
                                                         ) for k, v in self._pendentes.items()]
                                               ))
            else:
                self._console.addlin(ct.MSG_SEM_APOSTA_PEND)
                self._doc.section.append(
                    E.div({'class':'pendentes'}, E.h1(ct.TIT_PENDENTES), E.div({'class':'msg'}, ct.MSG_SEM_APOSTA_PEND)))

            [self._console.addbar() for _ in range(2)]
            self._doc.section.append(E.div(E.h1('aposte on-line: ',
                                                E.a('Loterias Online da Caixa',
                                                    href='http://loteriasonline.caixa.gov.br/'),
                                                E.div({'class':'msg'}))))

    def browser(self):
        self._salva_resumo()
        self._browse_resumo()

    def imprime_sorteios(self):
        """Imprime o último sorteio de cada modalidade."""
        
        for sorteio in self._sorteios:
            sorteio.imprimir()

    def imprime_pacotes(self):
        """Imprime todos os sorteios de todas as modalidades e suas respectivas apostas."""

        for pacote in self._pacotes:
            pacote.imprimir()

    def _salva_resumo(self):
        """Salva um objeto :class:`documento <documento.Doc>` em arquivos *html* individuais."""
        
        self._doc.salvar('resumo_{}.html'.format(ct.AGORA_NUM))

    def _browse_resumo(self):
        """Exibe um objeto :class:`documento <documento.Doc>` no browser padrão."""
        
        self._doc.browser()

    def injeta_sorteios(self):
        """Prepara os objetos :class:`documento <documento.Doc>` em arquivo *html* único."""
        
        for sorteio in self._sorteios:
            divsorteios = E.div({'class':'sorteios'})
            divsorteios.append(sorteio.doc.header) # body
            divsorteios.append(sorteio.doc.section)
            divsorteios.append(sorteio.doc.footer)
            self._doc.section.append(divsorteios)

    def injeta_pacotes(self):
        """Prepara lotes de objetos :class:`documento <documento.Doc>` em arquivo *html* único."""
        
        for pacote in self._pacotes:
            for sorteio in pacote._sorteios:
                divsorteios = E.div({'class':'sorteios'})
                divsorteios.append(sorteio.doc.header) # body
                divsorteios.append(sorteio.doc.section)
                divsorteios.append(sorteio.doc.footer)
                self._doc.section.append(divsorteios)

    def _obt_cache(self):
        try:
            shv = shelve.open(os.path.join(ct.MEUSDADOS, 'rcache'))
            if self._dvargs.get('i'):
                del shv['R']
            ultimos = shv['R']
            self._msgs.append(ct.MSG_OBTIDO_CACHE)
        except:
            ultimos = {}
        finally:
            shv.close()
            return ultimos

    def _grv_cache(self):
        with shelve.open(os.path.join(ct.MEUSDADOS, 'rcache'), writeback=True) as shv:
            shv['R'] = self._ultimos
            self._msgs.append(ct.MSG_OBTIDO_INTERNET)

    def _obt_pendentes(self):
        for k, v in self._ultimos.items():
            repetidos = [nconc for nconc in ut.sequenciar([lin[1] for lin in self._dvargs.get('csvdados') if lin[0].lower() == k]) if int(nconc) > int(v)]
            unicos = sorted(set(repetidos), key=int)
            pendentes = [(n, repetidos.count(n)) for n in unicos]
            if len(pendentes):
                self._pendentes[k] = pendentes

