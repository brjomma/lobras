﻿#!/usr/bin/python
# coding: utf-8
'''
Simula a quantidade de apostas e dezenas por modalidade de loteria.

As simulações são interessantes e aguçam o apetite pelo jogo, mas
não são o foco do sistema.
'''

__version__ = '0.1.0'

__copyright__ = 'Copyright 2018, @brjomma <brjomma@gmail.com>'
__license__ = 'MIT <https://mit-license.org/>'


import os
from lxml.builder import E #elementMaker
from simuladas import Simulada
from documento import Doc
from console import Console
import constantes as ct

   
class Simulacao(object):
    """Simula uma ou mais apostas variando quantidade e número de dezenas.
    
    :param dict dvargs: Um dicionário que contém os argumentos passados e as modalidades.
    :param list lst_dezenas: (opcional) Uma lista com uma ou várias entidades dezenas.
    :param list lst_ndez: (opcional) Uma lista com uma ou várias entidades número de dezenas.
    :param int q: (opcional) A quantidade de apostas a simular.

    **Exemplo:**
    O exemplo abaixo simula 10 apostas da **megasena**. 5 com 7 dezenas e 5 com 9 dezenas.
    
    - Dezenas fixas: 10-20.
    - Quantidade: 5.
    - Número de dezenas: 7 e 9.

        >>> br simula -d 10-20 -q 5 -n 7 9
        
    """
    
    def __init__(self, dvargs):
        self._dvargs = dvargs
        self._apostas = []
        self._doc = Doc()
        self._console = Console()
        self._add_simulada()
        self._montatela()
        self._montadoc()
        
    def _add_simulada(self):
        """Preenche o container ``self._apostas`` com as apostas."""
        
        #: :func:`abs` quantidade de apostas deve ser um positivo.
        #
        quant = 1 if self._dvargs['q'] <= 0 else self._dvargs['q']
        for _ in range(quant):
            for flat_dezenas in self._dvargs['lst_dezenas']:
                for ndez in self._dvargs['lst_ndez']:
                    aposta = Simulada(self._dvargs, flat_dezenas=flat_dezenas, ndez=ndez)
                    self._apostas.append(aposta)
        self._apostas.sort(key=lambda obj: (len(obj.dezenas.numeros), obj.dezenas.numeros))
        
    def _landing(self):
        return 'http://loterias.caixa.gov.br/wps/portal/loterias/landing/{}/'.format(self._dvargs['modal'])
        
    def _montatela(self):
        """Cria e organiza os dados para impressão na tela."""
        
        self._console.addbar('=')
        self._console.addlin(ct.TIT_SIMULACOES + '{}'.format(self._dvargs['modal']), 
                         {'texto':ct.DATA_HORA, 'alin':'dir'})
        self._console.addbar('=')
        
        for na, aposta in enumerate(self._apostas):
            self._console.addlist(aposta.montatela(na))
            self._console.addbar('-')
        self._console.addlin(ct.MSG_APOSTA_SIMULADA.format(len(self._apostas)))
        self._console.addbar()
        self._console.addbar()
                
    def _montadoc(self):
        """Produz um documento bem formatado em *html*."""
        
        self._doc.head = E.head(
                            E.title('Lobras, sim ' + self._dvargs['modal']), 
                            E.link(rel='stylesheet', href='../css/lobras.css', type='text/css'),
                            E.meta(name=ct.META_NAME))
        self._doc.header = E.header(E.h1(ct.TIT_SIMULACOES, E.a(self._dvargs['modal'], href=self._landing()),
                                    E.span(ct.DATA_HORA)))

        for na, aposta in enumerate(self._apostas):
            self._doc.section.append(aposta.montadoc(na))
        
        self._doc.footer = E.footer(E.div(ct.MSG_APOSTA_SIMULADA.format(len(self._apostas))))

    def imprimir(self):
        """Descarrega os dados do objeto :class:`console <console.Console>` na tela.
        
        Por padrão, a opção ``-H`` salva o :class:`documento <documento.Doc>` em um 
        arquivo *html* único e o exibe no browser padrão.
        """
        
        self._console.imprimir()
        
        if self._dvargs.get('H'):
            self._salvar()
            self._browser()

    def _salvar(self):
        self._doc.salvar('simulações_{}_{}_{}.html'.format(self._dvargs['modal'], len(self._apostas), ct.AGORA_NUM))
        
    def _browser(self):
        self._doc.browser()
