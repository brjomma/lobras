﻿#!/usr/bin/python
# coding: utf-8
"""Objetos que definem os números de simulação.

Os objetos assumem  características diferentes de acordo com a modalidade.

"""

__version__ = '0.1.0'

__copyright__ = 'Copyright 2018, @brjomma <brjomma@gmail.com>'
__license__ = 'MIT <https://mit-license.org/>'


import random
from lxml.builder import E #elementMaker
import utils as ut
import apifederal
import constantes as ct
import modals


BLOCO = 'display: inline-block;'


class Simulada(object):
    """Aposta simulada que não é conferida.
    
    :param dict dvargs: Um dicionário que contém os argumentos passados e as modalidades.
    :param str flat_dezenas: (opcional) Uma entidade encadeada de strings representando as dezenas do coração. ``'15-16-25'``.
    :param int ndez: (opcional) Um numeral com o valor da quantidade de dezenas.
        
    """
    
    def __init__(self, dvargs, flat_dezenas, ndez):
        Obj = objs[dvargs['modals'].index(dvargs['modal'])]
        self.dezenas = Obj(flat_dezenas=flat_dezenas, ndez=ndez, dvargs=dvargs)

    def montatela(self, na):
        '''
        Produz uma lista com fragmentos de dados de impressão da simulação.
        
        :param int na: Um numeral representando a ordem da aposta corrente.
        :return: :class:`list`.
        
        Será incorporada à :class:`console <console.Console>` principal.
        '''
        
        frag = []
        frag.append(('Aposta', 
                    str(na+1).zfill(3),
                    {'texto':ct.INFO_MARCACOES.format(len(self.dezenas.numeros)), 
                    'alin':'dir'}))
        frag.extend(self.dezenas.montatela())
        [frag.append((msg,)) for msg in self.dezenas._msgs]
        return frag
        
    def montadoc(self, na):
        '''
        Produz uma lista com fragmentos de dados *html* referente à simulação.
        
        :param int na: Um numeral representando a ordem da aposta corrente.
        :return: :class:`list`.
        
        Será incorporado ao :class:`documento <documento.Doc>` principal.
        '''
        
        frag = E.div({'class':'aposta'},
                E.table(
                    E.thead(
                        E.tr(E.th('Aposta'), 
                              E.th(str(na+1).zfill(3)),
                              E.th(ct.INFO_MARCACOES.format(len(self.dezenas.numeros))) )
                              ),
                    E.tbody(*self.dezenas.montadoc()),                              
                    E.tfoot(
                            *[E.tr(E.th(msg, colspan='3')) for msg in self.dezenas._msgs]
                            )
                ))
        return frag
                            

class Dezenas(modals.Dezenas, object):
    """Objetos gerados aleatoriamente considerando escolhas fixas..
    
    :param str args: A modalidade da loteria.
    :param str flat_dezenas: Uma cadeia de strings representando as dezenas da aposta. ``"15-16-19-25-26-25"``.
    :param int ndez: Um numeral com o valor da quantidade de dezenas.
    """

    def __init__(self, **kwargs):
        super().__init__()
        self._dvargs = kwargs['dvargs']
        self._modal = self._dvargs['modal']
        self._flatdezenas = kwargs.get('flat_dezenas') #vai sair
        self._ndez = kwargs.get('ndez') or self.__class__.min
        self._msgs = []
        self._lst_prevalidas = []
        self.numeros = []


        self._fatiar()
        self._prevalidar()
        self._randomizar()

    def _randomizar(self):
        preval = self._lst_prevalidas
        if len(preval):
            self._msgs.append(' - ({}) dezena(s) fixa(s) [{}]'.format(len(preval), '-'.join(preval)))
            
        # confere display de simulação
        #
        if len(preval) > self.__class__.max:
            self._msgs.append(ct.MSG_NUM_DEZ_INVALIDO_VER.format(len(preval), self.__class__.min, self.__class__.max))
            
        if self._ndez > self.__class__.max: 
            self._msgs.append(' - núm. de dezenas inválido ({}), usando máx: ({})'.format(self._ndez, self.__class__.max))
            self._ndez = self.__class__.max
        elif self._ndez < self.__class__.min:  
            self._msgs.append(' - núm. de dezenas inválido ({}), usando min: ({})'.format(self._ndez, self.__class__.min))
            self._ndez = self.__class__.min

        if self._ndez > len(preval):
            populacao = range(1, self.__class__.display+1)
            conflito = True
            while conflito:
                rands = random.sample(populacao,  self._ndez - len(preval))
                lst_randomicas = [str(dz).zfill(2) for dz in rands]
                conflito = len(set(lst_randomicas) & set(preval)) > 0        
            self.numeros = preval + lst_randomicas  # não pode ser :extend() quando é nula

        # dezenas numéricas devem ser ordenadas
        #        
        self.numeros.sort(key=int)

    def montatela(self):
        """Produz um fragmento em forma de lista correspondente às dezenas simuladas.
        
        Este fragmento será incorporado ao objeto :class:`console <console.Console>` principal.
        
        :return: :class:`list`
        """
        
        return [({'texto':'-'.join(self.numeros), 'alin':'dir'},)]

    def montadoc(self):
        """Produz uma lista de fragmentos *html* das dezenas simuladas.

        Estes fragmentos serão incorporados ao objeto :class:`documento <documento.Doc>` principal.

        :return: :class:`list`
        """
        
        return [
                E.tr(E.td(
                        E.ul({'class':'ul_simuladas '}, 
                        *[E.li('{}'.format(dz), style=BLOCO) for dz in self.numeros]
                        ),
                        colspan='3' ))
               ]                


class Lotogol(modals.Lotogol, Dezenas):

    def _randomizar(self):
        if self.__class__.max > len(self._lst_prevalidas):  
            lst_randomicas = []
            for _ in range(self.__class__.max - len(self._lst_prevalidas)):
                lst_randomicas.append(random.choice(self.__class__.display))
            # randomicas = random.sample(placar,  self.__class__.max - len(self._lst_prevalidas)) sem repetição?
            lst_misturadas = self._lst_prevalidas + lst_randomicas
            random.shuffle(lst_misturadas)
            self.numeros = lst_misturadas
        if len(self._lst_prevalidas):
            self._msgs.append(ct.MSG_MARC_FIXAS.format(len(self._lst_prevalidas), '-'.join(self._lst_prevalidas)))
        
        # confere display de simulação
        #
        if len(self._lst_prevalidas) > self.__class__.max:
            self._msgs.append(ct.MSG_MARC_MIN_MAX.format(len(self._lst_prevalidas), self.__class__.min, self.__class__.max))


class Loteca(modals.Loteca, Dezenas):

    def _randomizar(self):
        preval = self._lst_prevalidas
        lst_duplos = [x for x in preval if len(x) == 2] or self._cria_duplos(self._dvargs.get('D', 0))
        lst_triplos = [x for x in preval if len(x) == 3] or self._cria_triplos(self._dvargs.get('T', 0))
        
        sucesso, msg = self._valida_extras(len(lst_duplos), len(lst_triplos))
        if not sucesso and lst_duplos == [] and lst_triplos == []:
            lst_duplos = self._cria_duplos(1)
            lst_triplos = []
            msg = ' - 1 duplo'
        self._msgs.append(msg)

        if len(preval):
            self._msgs.append(ct.MSG_MARC_FIXAS.format(len(preval), '-'.join(preval)))

        if len(preval) > self.__class__.max:
            self._msgs.append(ct.MSG_MARC_MIN_MAX.format(len(preval), self.__class__.min, self.__class__.max))

        tam = len(preval) or (len(lst_duplos) + len(lst_triplos))
        # confere display de simulação
        #
        if self.__class__.max > tam:
            lst_misturadas = [random.choice(self.__class__.display) for _ in range(self.__class__.max - tam)] + lst_duplos + lst_triplos
            random.shuffle(lst_misturadas)
            self.numeros = lst_misturadas

    def _cria_duplos(self, q):
        return [random.choice(['02','10','12']) for n in range(q)]

    def _cria_triplos(self, q):
        return ['102' for n in range(q)]

        
class Megasena(modals.Megasena, Dezenas):
    pass


class Duplasena(modals.Duplasena, Dezenas):
    pass


class Diadesorte(modals.Diadesorte, Dezenas):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._obt_mesdasorte(kwargs['dvargs'].get('t'))

    def montatela(self):
        frag = super().montatela()
        # frag.append(({'texto':'-'.join(self.numeros), 'alin':'dir'},))
        frag.append((' - mês da sorte', {'texto':str(self._mesindex) + ' ' + self._mesdasorte, 'alin':'dir'}))
        return frag

    def montadoc(self):
        frag = [E.tr(E.td(' - mês da sorte', colspan='2'), E.td(str(self._mesindex+1) + ' ' + self._mesdasorte))]
        frag.extend(super().montadoc())
        return frag


class Lotofacil(modals.Lotofacil, Dezenas):
    pass

    
class Lotomania(modals.Lotomania, Dezenas):

    def montatela(self):
        return [
                ({'texto':'-'.join(self.numeros[:25]), 'alin':'dir'},),
                ({'texto':'-'.join(self.numeros[25:]), 'alin':'dir'},)
        ]

    def montadoc(self):
        return [
                E.tr(E.td(
                        E.ul({'class':'ul_simuladas '}, 
                        *[E.li('{}'.format(dz), style=BLOCO) for dz in self.numeros[:25]]
                    ),
                        colspan='3' )),
                E.tr(E.td(
                        E.ul({'class':'ul_simuladas '}, 
                        *[E.li('{}'.format(dz), style=BLOCO) for dz in self.numeros[25:]]
                    ),
                        colspan='3' ))
               ]


class Quina(modals.Quina, Dezenas):
    pass

    
class Timemania(modals.Timemania, Dezenas):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self._obt_timecoracao(kwargs['dvargs'].get('t'))
        
    def montatela(self):
        frag = super().montatela()
        frag.append((' - time do coração', {'texto':str(self._timeindex) + ' ' + self._timecoracao, 'alin':'dir'}))
        return frag

    def montadoc(self):
        frag = [E.tr(E.td(' - time do coração', colspan='2'), E.td(str(self._timeindex+1) + ' ' + self._timecoracao))]
        frag.extend(super().montadoc())
        return frag


class Federal(modals.Federal, Dezenas):

    def _randomizar(self):
        preval = self._lst_prevalidas
        if len(preval):
            self._msgs.append(' - ({}) dígito(s) fixo(s) [{}]'.format(len(preval), ''.join(preval)))

        
        if self.__class__.max > len(preval):
            lst_randomicas = []
            for _ in range(self.__class__.max - len(preval)):
                lst_randomicas.append(str(random.choice(range(0, 10))))
            # randomicas = random.sample(placar,  self.__class__.max - len(preval)) sem repetição?
            lst_misturadas = preval + lst_randomicas
            bil = self.__class__.display
            while bil >= self.__class__.display:
                random.shuffle(lst_misturadas)
                bil = int(('').join(lst_misturadas))
            self.numeros = lst_misturadas
        # confere display de simulação
        #
        if len(preval) > self.__class__.max:
            self._msgs.append(' - núm. do bilhete inválido ({}), verifique! (min: {}, máx: {})'.format(len(preval), self.__class__.min, self.__class__.max))

    def montatela(self):
        return [({'texto':''.join(self.numeros), 'alin':'dir'},)]


objs = [Megasena, Duplasena, Diadesorte , Lotofacil, Lotomania, Quina, Timemania, Lotogol, Loteca, Federal]

