﻿#!/usr/bin/python
# coding: utf-8
"""Objeto que realiza consultas e produz um resultado.

Este objeto faz uma consulta no *cache* ou na *internet* e organiza
as informações do concurso sorteado. Também possui um `container` para gerenciar objetos :class:`apostas <apostadas.Apostada>`.

"""

__version__ = '0.1.0'

__copyright__ = 'Copyright 2018, @brjomma <brjomma@gmail.com>'
__license__ = 'MIT <https://mit-license.org/>'


import os
import datetime
import locale
import shelve
import requests
from lxml.builder import E #elementMaker
import utils as ut
from apostadas import Apostada
from documento import Doc
from console import Console
import constantes as ct


locale.setlocale(locale.LC_ALL, 'pt_BR.UTF-8')


def _strTofloat(n):
    """O retorno das informações vem em formatos diferentes para a mesma variável.
    
    A informação de valor às vezes vem em formato de número e outras 
    vezes em formato de string. Esta função corrige estritamente o problema.
    
    """    
    
    if isinstance(n, str):
        return float(n.replace('.','').replace(',','.'))
    return n

class Sorteio(object):
    """Confere um ou mais resultados de cada modalidade informada.
    
    :param dict dvargs: Um dicionário que contém os argumentos passados e as modalidades.
    :param str modal: (opcional) A modalidade atual extraída da lista de modalidades.
    :param list lst_dezenas: (opcional) Uma lista com uma ou várias entidades dezenas.
    :param str nconcurso: (opcional) Um string representando o número do concurso, se for :mod:`None` retornará o último concurso.

    """
            
    def __init__(self, dvargs, **kwargs):
        self.dvargs = dvargs
        self.modal = self.dvargs['modal']
        self.indexmodal = self.dvargs['modals'].index(self.modal)
        self._lst_dezenas = kwargs.get('lst_dezenas') or [None]
        self.concurso = kwargs.get('nconcurso')
        self._apostas = []
        self._msgs = []
        self._console = Console()
        self.doc = Doc()
        self._datasort = ''
        self.sorteadas = []
        self.rateios = []
        self.premiadas = []
        self._alert = None

        cache = self._obt_cache()
        if cache == {}:
            cache = self._obt_web()

        if not self._alert:
            self.concurso = cache['nconcurso']
            self._datasort = cache['datasort']
            self.sorteadas = cache['sorteadas']
            self.premiacao = cache['premiacao']
            self.rateios = cache['rateios']
            self.acumulado = cache['acumulado']
            self._extra = cache.get('extra', None)
            self.proximo = cache['proximo']
            
            # deve vir antes da validade por causa do (-V in args)
            self._add_apostada()  
            self._obt_validade()
            for filename in self.dvargs.get('csverros', []):
                self._msgs.append(ct.MSG_ARQ_NAO_ENCONTRADO.format(filename))

        self._montatela()
        self._montadoc()

    def _monta_url(self):
        base = 'http://loterias.caixa.gov.br/wps/portal/loterias/!ut/p/a1/'
        valor = 'dl5/d5/L2dBISEvZ0FBIS9nQSEh/pw/{}/res/id=buscaResultado/c=djonLevelPage/=/'.format(self.__class__.token)
        busca = '?timestampAjax={}&concurso={}'.format(ct.AGORA_NUM, self.concurso) if self.concurso is not None else ''
        return base + self.__class__.verificador + valor + busca        
        
    def _add_apostada(self):
        for str_dezenas in self._lst_dezenas:
            if str_dezenas:
                aposta = Apostada(str_dezenas, self)
                self._apostas.append(aposta)
                
    def _obt_validade(self):
        """Calcula a data de validade do sorteio.
        
        Retorna a data-limite com base em 90 dias, conforme as regras da Caixa.
        """
        
        #print 'Somando-se 3 meses:', (agora + datetime.timedelta(days=90)).strftime('%d/%m/%Y')
        #https://wiki.python.org.br/FormatarDatas
        delta = datetime.timedelta(days=90)
        vdatasort = [int(x) for x in reversed(self._datasort.split('/'))]
        datasort = datetime.date(*vdatasort)
        hoje = datetime.date.today()
        limite = datasort + delta
        self._validade = limite.strftime('%d/%m/%Y') 

        # -V: força a conferência de concurso vencido
        #
        if len(self._apostas):
            if not self.dvargs.get('V'):
                if limite < hoje:
                    self._alert = ct.MSG_CONF_NAO_RECOMENDADA
            else:
                if hoje > limite:
                    self._msgs.append(ct.MSG_CONF_FORCADA)

    def _montatela(self):
        """Cria uma estrutura de impressão."""
        
        indicador = ('==>', 10, 'dir')
        self._console.addbar('=')
        self._console.addlin(ct.TIT_RESULTADOS + '{}'.format(self.modal), 
                         {'texto':ct.DATA_HORA, 'alin':'dir'})
        self._console.addbar('=')
        self._console.addlin(('Concurso', 24), indicador, dict(texto=self.concurso, alin='dir'))
        if self._alert:
            self._console.addlin(self._alert)
        else:
            self._console.addlin(('Data do sorteio', 24), indicador, dict(texto='{} {}'.format(ut.dia_abrev(self._datasort), self._datasort), alin='dir'))
            if self.acumulado != 'R$ 0,00':
                self._console.addlin(('Acumulado', 24), indicador, dict(texto=self.acumulado, alin='dir'))
            self._console.addbar('-')

            for ns, resultado in enumerate(self.sorteadas):
                str_sort = ' {}'.format(ns+1) if len(self.sorteadas) > 1 else ''
                
                # se o tamanho da cadeia de caracteres for grande
                # será apresentado na linha seguinte
                # lotofácil e lotomania
                #
                if len('-'.join(resultado)) <= 35: 
                    self._console.addlin((ct.TIT_RESULTADO_SORTEIO + str_sort, 24), indicador, dict(texto='-'.join(resultado), alin='dir'))
                else:
                    self._console.addlin(ct.TIT_RESULTADO_SORTEIO + str_sort)
                    self._console.addlin(dict(texto='-'.join(resultado), alin='dir'))
                
                if self._extra:
                    self._console.addlin((self._extra[0], 24), indicador, dict(texto=self._extra[1], alin='dir'))
                self._console.addbar('-')
                
                self._console.addlin('Premiação')
                for n, premiacao in enumerate(self.premiacao):
                    self._console.addlin((' '+ premiacao, 24), (self.rateios[ns][n][0], 10, 'dir'), dict(texto=self.rateios[ns][n][1], alin='dir'))
                self._console.addbar('-')
        
            for na, aposta in enumerate(self._apostas):
                self._console.addlist(aposta.montatela(na))
                self._console.addbar('-')
                    
            if len(self._apostas):
                self._console.addlin(ct.MSG_APOSTA_CONFERIDA.format(len(self._apostas)))
            else:
                self._console.addlin(ct.MSG_SEM_APOSTAS)
                
            self._console.addlin(ct.MSG_VALID_CONCURSO.format(ut.dia_abrev(self._validade), self._validade))

            self._console.extendlist(self._msgs)
                
        self._console.addbar()
        self._console.addbar()
                
    def imprimir(self):
        """Imprime o objeto :class:`console <console.Console>` na tela."""
        
        self._console.imprimir()

    def _montadoc(self):
        """Produz um conteúdo em *html*."""
        
        self.doc.head = E.head(
                            E.title('Lobras, res ' + self.modal), 
                            E.link(rel='stylesheet', href='../css/lobras.css', type='text/css'),
                            E.meta(name=ct.META_NAME))
                   
        self.doc.header = E.header(E.h1(ct.TIT_RESULTADOS, E.a(self.modal, href=ct.LANDING.format(self.modal)),
                                   E.span(ct.DATA_HORA)))
        tbody_concurso = E.tbody(
                            E.tr(
                                E.td('Concurso'),
                                # self.concurso: lxml não aceita tipo None
                                #
                                E.td(E.span({'class':'n_concurso'}, self.concurso if self.concurso else 'n/info')))
                              )
        self.doc.section.append(E.div({'class':'concurso'}, E.table(tbody_concurso)))
        
        if self._alert:
            self.doc.footer.append(E.div(self._alert))
        else:
            tbody_concurso.append(
                    E.tr(E.td('Data do sorteio'), 
                          E.td('{} {}'.format(ut.dia_abrev(self._datasort), self._datasort)))
                         )
            if self.acumulado != 'R$ 0,00':
                tbody_concurso.append(
                    E.tr(E.td('Acumulado'), 
                          E.td(E.span(self.acumulado)))
                            )
            self.doc.section.append(E.div({'class':'sorteio'},
                            *[E.div(
                            E.table({'class':'resultado'},
                                E.tbody(
                                    E.tr(
                                        E.td(ct.TIT_RESULTADO_SORTEIO + ' {}'.format(ns+1) if len(self.sorteadas) > 1 else ct.TIT_RESULTADO_SORTEIO),
                                        E.td(
                                            E.ul({'class':'ul_sorteadas ' + self.modal},
                                            *[E.li(dz, style='display: inline-block;') for dz in resultado]
                                            ))),
                                    *[E.tr(
                                        E.td(ext[0]),
                                        E.td(ext[1])) for ext in [self._extra] if self._extra]
                                        )
                                        ),
                            E.table({'class':'premiacao'},
                                E.thead(
                                    E.tr(
                                        E.th('Premiação', colspan='3'))),
                                E.tbody(
                                    *[E.tr(
                                        E.td(self.premiacao[n]),
                                        E.td(rate[0]),
                                        E.td(rate[1]) ) for n, rate in enumerate(self.rateios[ns])])
                                        )) for ns, resultado in enumerate(self.sorteadas)]
                                        
                                        ))
            
            for na, aposta in enumerate(self._apostas):
                self.doc.section.append(aposta.montadoc(na))
            
            self.doc.footer = E.footer(
                  E.div(ct.MSG_APOSTA_CONFERIDA.format(len(self._apostas)) if len(self._apostas) else ct.MSG_SEM_APOSTAS),
                  E.div(ct.MSG_VALID_CONCURSO.format(ut.dia_abrev(self._validade), self._validade)),
                  *[E.div(msg) for msg in self._msgs]
                                    )
        
    def salvar(self):
        """Grava o objeto `lxml <https://lxml.de/>`_ em um arquivo."""
        
        self.doc.salvar('resultado_{}_{}.html'.format(self.modal, self.concurso))
        
    def browser(self):
        """Exibe o arquivo *html* no browser padrão."""
        
        self.doc.browser()
       
    def _obt_cache(self):
        json = {}
        
        # deletar o cache ``-X``
        #
        if self.dvargs.get('X'):
            os.remove(os.path.join(ct.MEUSDADOS, 'rcache.dat'))
            os.remove(os.path.join(ct.MEUSDADOS, 'rcache.bak'))
            os.remove(os.path.join(ct.MEUSDADOS, 'rcache.dir'))
            self._msgs.append(' - o cache foi renovado')
            
        shv = shelve.open(os.path.join(ct.MEUSDADOS, 'rcache'))
        
        try:
            datmodal = shv[self.modal]
        except:
            datmodal = shv[self.modal] = {}
        finally:
            shv.close()
            
        try:
            # força a consulta na internet ``/i``
            #
            if self.dvargs.get('i') or self.concurso is None:
                del datmodal[self.concurso]
            json = datmodal[self.concurso]
            self._msgs.append(ct.MSG_OBTIDO_CACHE)
        finally:
            return json
    
    def _obt_web(self):
        rqjson = {}
        
        try:
            rq = requests.get(url=self._monta_url(), timeout=5) 
            if rq.status_code != 200:
                rq.raise_for_status()
            else:
                rqjson = self._equalizar(rq.json())
                if rqjson['alert']:
                    self._alert = ' - concurso inválido ou não sorteado, verifique!'
                else:
                    with shelve.open(os.path.join(ct.MEUSDADOS, 'rcache'), writeback=True) as shv:
                        datmodal = shv[self.modal]

                        # sempre assume o último concurso efetivamente consultado
                        #
                        nconc = rqjson['nconcurso']
                        datmodal[nconc] = rqjson
                    self._msgs.append(ct.MSG_OBTIDO_INTERNET + ', {}ms'.format(rq.elapsed.microseconds))
        except requests.exceptions.HTTPError:
            self._alert = ' - erro interno no servidor ({}), tente mais tarde'.format(rq.status_code)
        except requests.exceptions.ConnectionError:
            self._alert = ' - erro de conexão, tente mais tarde'
        except requests.exceptions.Timeout:
            self._alert = ' - erro de timeout, tente mais tarde'
        except requests.exceptions.RequestException:
            pass

        finally:
            return rqjson

    
class Megasena(Sorteio):
    token = 'Z7_HGK818G0KO6H80AU71KG7J0072'
    verificador = '04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbwMPI0sDBxNXAOMwrzCjA0sjIEKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wNnUwNHfxcnSwBgIDUyhCvA5EawAjxsKckMjDDI9FQE-F4ca/'

    def _equalizar(self, json):
        r=[('ganhadores','valor'), ('ganhadores_quina','valor_quina'), ('ganhadores_quadra','valor_quadra')] 
        
        if json != {}:
            equal = {}
            equal['alert'] = json['error']

            if not equal['alert']:
                equal['premiacao'] = ['sena', 'quina', 'quadra']
                equal['nconcurso'] = str(json['concurso'])
                equal['datasort'] = json['dataStr']
                equal['sorteadas'] = [json['resultadoOrdenado'].split('-')]
                equal['rateios'] = [[(str(json[tup[0]] or 0), locale.currency(json[tup[1]] or 0, grouping = True)) for tup in r]]
                equal['acumulado'] = locale.currency(json['valor_acumulado'] or 0, grouping = True)
                equal['proximo'] = json['dt_proximo_concursoStr']
            
            return equal
        return json


class Federal(Sorteio):
    token = 'Z7_61L0H0G0J0VSC0AC4GLFAD20G0'
    verificador = '04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbz8vTxNDRy9_Y2NQ13CDA0MzIAKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wBmoxN_FydLAGAgNTKEK8DkRrACPGwpyQyMMMj0VAYe29yM!/'

    def _equalizar(self, json):
    
        def _prox_federal():
            import datetime
            hj = datetime.date.today()
            dds = datetime.date.weekday(hj)
            if dds == 0: dias = 2
            elif dds == 1: dias = 1
            elif dds == 2: dias = 3
            elif dds == 3: dias = 2
            elif dds == 4: dias = 1
            elif dds == 5: dias = 4
            else: dias = 3
            return (hj + datetime.timedelta(days=dias)).strftime('%d/%m/%Y')
    
        if json != {}:
            equal = {}

            # try: na federal quando não acha extração o json vem modificado
            #
            try:
                equal['alert'] = json['premios'][0]['error']
            except:
                equal['alert'] = json['error']

            if not equal['alert']:
                equal['premiacao'] = ['1\u00BA Prêmio', '2\u00BA Prêmio', '3\u00BA Prêmio', '4\u00BA Prêmio',
                                      '5\u00BA Prêmio']
                equal['nconcurso'] = json['concurso']
                equal['datasort'] = json['data']
                equal['sorteadas'] = [[p['bilhete'] for p in json['premios']]]
                equal['rateios'] = [[(p['bilhete'], locale.currency(_strTofloat(p['valor']), grouping = True)) for p in json['premios']]]
                equal['acumulado'] = locale.currency(0, grouping = True)
                equal['proximo'] = _prox_federal()
                equal['qbilhetes'] = int(json['premios'][0]['qtde_bilhetes'])
            
            return equal
        return json


class Duplasena(Sorteio):
    token = 'Z7_61L0H0G0J0I280A4EP2VJV30N4'
    verificador = '04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbwMPI0sDBxNXAOMwrzCjA2cDIAKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wNnUwNHfxcnSwBgIDUyhCvA5EawAjxsKckMjDDI9FQGgnyPS/'

    def _equalizar(self, json):
        r1=[('ganhadores_sena1','valor_sena1'), ('qt_ganhador_quina_faixa1','vr_quina_faixa1'), ('qt_ganhador_quadra_faixa1','vr_quadra_faixa1'), ('qt_ganhador_terno_faixa1','vr_terno_faixa1')]
        r2=[('ganhadores_sena2','valor_sena2'), ('ganhadores_quina2','valor_quina2'), ('ganhadores_quadra2','valor_quadra2'), ('qt_ganhador_terno_faixa2','vr_terno_faixa2')]
        
        if json != {}:
            equal = {}
            equal['alert'] = json['error']

            if not equal['alert']:
                equal['premiacao'] = ['sena', 'quina', 'quadra', 'terno']
                equal['nconcurso'] = str(json['concurso'])  # entrega "concurso" numérico
                equal['datasort'] = json['dataStr']
                equal['sorteadas'] = [json['resultadoOrdenadoSorteio1'].split('-'), json['resultadoOrdenadoSorteio2'].split('-')]
                equal['rateios'] = [[(str(json[tup[0]] or 0), locale.currency(json[tup[1]] or 0, grouping = True)) for tup in r1],[(str(json[tup[0]]), locale.currency(json[tup[1]], grouping = True)) for tup in r2]]
                equal['acumulado'] = locale.currency(json['valor_acumulado_sena1'] or 0, grouping = True)
                equal['proximo'] = json['data_proximo_concursoStr']
            
            return equal
        return json

        
class Diadesorte(Sorteio):
    token = 'Z7_HGK818G0KO5GE0Q8PTB11800G3'
    verificador = 'jc5BDsIgFATQs3gCptICXdKSfpA2ujFWNoaVIdHqwnh-sXFr9c_qJ2-SYYGNLEzxmc7xkW5TvLz_IE6WvCoUwZPwArpTnZWD4SCewTGDlrQtZQ-gVGs401gj6wFw4r8-vpzGr_6BhZmIoocFYUO7toLemqYGz0H1AUsTZ7Cw4X7dj0hu9QIyUWUw/'

    def _equalizar(self, json):
        r = [('qt_GANHADOR_FAIXA_1','vr_RATEIO_FAIXA_1'), ('qt_GANHADOR_FAIXA_2','vr_RATEIO_FAIXA_2'), ('qt_GANHADOR_FAIXA_3','vr_RATEIO_FAIXA_3'), ('qt_GANHADOR_FAIXA_4','vr_RATEIO_FAIXA_4'), ('qt_GANHADOR_MES_DE_SORTE','vr_RATEIO_MES_DE_SORTE')]
        meses = ["JANEIRO", "FEVEREIRO", "MARÇO", "ABRIL", "MAIO", "JUNHO",
                 "JULHO", "AGOSTO", "SETEMBRO", "OUTUBRO", "NOVEMBRO", "DEZEMBRO"]

        if json != {}:
            equal = {}
            equal['alert'] = json['error']

            if not equal['alert']:
                equal['premiacao'] = ['7 acertos', '6 acertos', '5 acertos', '4 acertos', 'mês da sorte']
                equal['nconcurso'] = json['nu_CONCURSO']
                equal['datasort'] = json['dt_APURACAOStr']
                equal['sorteadas'] = [json['resultadoOrdenado'].split('-')]
                equal['rateios'] = [[(str(json[tup[0]] or 0), locale.currency(json[tup[1]] or 0, grouping = True)) for tup in r]]
                equal['acumulado'] = locale.currency(json['vr_ACUMULADO'] or 0, grouping = True)
                equal['extra'] = ['Mês da sorte', meses[json['mes_DE_SORTE'] - 1]]
                equal['proximo'] = json['dt_PROXIMO_CONCURSOStr']

            return equal
        return json


class Lotofacil(Sorteio):
    token = 'Z7_61L0H0G0J0VSC0AC4GLFAD2003'
    verificador = '04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbz8vTxNDRy9_Y2NQ13CDA0sTIEKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wBmoxN_FydLAGAgNTKEK8DkRrACPGwpyQyMMMj0VAcySpRM!/'

    def _equalizar(self, json):
        r = [('qt_ganhador_faixa1', 'vr_rateio_faixa1'), ('qt_ganhador_faixa2', 'vr_rateio_faixa2'),
                   ('qt_ganhador_faixa3', 'vr_rateio_faixa3'), ('qt_ganhador_faixa4', 'vr_rateio_faixa4'),
                   ('qt_ganhador_faixa5', 'vr_rateio_faixa5')]

        if json != {}:
            equal = {}
            equal['alert'] = json['error']

            if not equal['alert']:
                equal['premiacao'] = ['15 acertos', '14 acertos', '13 acertos', '12 acertos', '11 acertos']
                equal['nconcurso'] = json['nu_concurso']
                equal['datasort'] = json['dt_apuracaoStr']
                equal['sorteadas'] = [json['resultadoOrdenado'].split('-')]
                equal['rateios'] = [[(str(json[tup[0]] or 0), locale.currency(json[tup[1]] or 0, grouping=True)) for tup in r]]
                equal['acumulado'] = locale.currency(_strTofloat(json['vrAcumuladoFaixa1']) or 0, grouping=True)
                equal['proximo'] = json['dtProximoConcursoStr']

            return equal
        return json


class Lotomania(Sorteio):
    token = 'Z7_61L0H0G0JGJVA0AKLR5T3K00V0'
    verificador = '04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbz8vTxNDRy9_Y2NQ13CDA38jYEKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wBmoxN_FydLAGAgNTKEK8DkRrACPGwpyQyMMMj0VAajYsZo!/'

    def _equalizar(self, json):
        # pegadinha da lotomania
        # a faixa de quinze acertos é a 7, pois antes não existia
        # a faixa de 0 acerto é a 6
        # concursos antigos até o 1643 retornam None -  str(json[tup[0]] or 0)
        #
        r = [('qtGanhadoresFaixa1', 'vrRateioFaixa1'), ('qtGanhadoresFaixa2', 'vrRateioFaixa2'),
              ('qtGanhadoresFaixa3', 'vrRateioFaixa3'), ('qtGanhadoresFaixa4', 'vrRateioFaixa4'),
              ('qtGanhadoresFaixa5', 'vrRateioFaixa5'),
              ('qtGanhadoresFaixa7', 'vrRateioFaixa7'), ('qtGanhadoresFaixa6', 'vrRateioFaixa6')]

        if json != {}:
            equal = {}
            equal['alert'] = json['error']

            if not equal['alert']:
                equal['premiacao'] = ['20 acertos', '19 acertos', '18 acertos', '17 acertos', '16 acertos',
                                      '15 acertos', '0  acerto']
                equal['nconcurso'] = str(json['concurso'])
                equal['datasort'] = json['dtApuracaoStr']
                equal['sorteadas'] = [json['resultadoOrdenado'].split('-')]
                equal['rateios'] = [[(str(json[tup[0]] or 0), locale.currency(json[tup[1]] or 0, grouping=True)) for tup in r]]
                equal['acumulado'] = locale.currency(json['vrAcumuladoFaixa1'] or 0, grouping=True)
                equal['proximo'] = json['dtProximoConcursoStr']

            return equal
        return json


class Quina(Sorteio):
    token = 'Z7_61L0H0G0J0VSC0AC4GLFAD20G6'
    verificador = 'jc69DoIwAATgZ_EJepS2wFgoaUswsojYxXQyTfgbjM9vNS4Oordd8l1yxJGBuNnfw9XfwjL78dmduIikhYFGA0tzSFZ3tG_6FCmP4BxBpaVhWQuA5RRWlUZlxR6w4r89vkTi1_5E3CfRXcUhD6osEAHA32Dr4gtsfFin44Bgdw9WWSwj/'

    def _equalizar(self, json):
        # pegadinha da quina, a faixa duque foi criada em 16/05/2016
        # concursos antigos retornam None -  str(json[tup[0]] or 0)

        r = [('ganhadores', 'valor'), ('ganhadores_quadra', 'valor_quadra'), ('ganhadores_terno', 'valor_terno'),
             ('qt_ganhador_duque', 'vr_rateio_duque')]

        if json != {}:
            equal = {}
            equal['alert'] = json['error']

            if not equal['alert']:
                equal['premiacao'] = ['quina', 'quadra', 'terno', 'duque']
                equal['nconcurso'] = str(json['concurso'])
                equal['datasort'] = json['dataStr']
                equal['sorteadas'] = [json['resultadoOrdenado'].split('-')]
                equal['rateios'] = [[(str(json[tup[0]] or 0), locale.currency(json[tup[1]] or 0, grouping=True)) for tup in r]]
                equal['acumulado'] = locale.currency(json['vrAcumulado'] or 0, grouping=True)
                equal['proximo'] = json['dtProximoConcursoStr']

            return equal
        return json


class Timemania(Sorteio):
    token = 'Z7_61L0H0G0JGJVA0AKLR5T3K00M4'
    verificador = '04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbz8vTxNDRy9_Y2NQ13CDA1MzIEKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wBmoxN_FydLAGAgNTKEK8DkRrACPGwpyQyMMMj0VASrq9qk!/'

    def _equalizar(self, json):
        r = [('qt_GANHADOR_FAIXA_1', 'vr_RATEIO_FAIXA_1'), ('qt_GANHADOR_FAIXA_2', 'vr_RATEIO_FAIXA_2'),
              ('qt_GANHADOR_FAIXA_3', 'vr_RATEIO_FAIXA_3'), ('qt_GANHADOR_FAIXA_4', 'vr_RATEIO_FAIXA_4'),
              ('qt_GANHADOR_FAIXA_5', 'vr_RATEIO_FAIXA_5'), ('qt_GANHADOR_TIME_CORACAO', 'vr_RATEIO_TIME_CORACAO')]

        if json != {}:
            equal = {}
            equal['alert'] = json['error']

            if not equal['alert']:
                equal['premiacao'] = ['7 acertos', '6 acertos', '5 acertos', '4 acertos', '3 acertos',
                                      'time do coração']
                equal['nconcurso'] = json['nu_CONCURSO']
                equal['datasort'] = json['dt_APURACAOStr']
                equal['sorteadas'] = [json['resultadoOrdenado'].split('-')]
                equal['rateios'] = [[(str(json[tup[0]] or 0), locale.currency(json[tup[1]] or 0, grouping=True)) for tup in r]]
                equal['acumulado'] = locale.currency(json['vr_ACUMULADO_FAIXA_1'] or 0, grouping=True)
                equal['extra'] = ['Time do coração', json['timeCoracao']]
                equal['proximo'] = json['dt_PROXIMO_CONCURSOStr']

            return equal
        return json


class Lotogol(Sorteio):
    token = 'Z7_HGK818G0KGBQD0ARPT1QLG1041'
    verificador = '04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbwMPI0sDBxNXAOMwrzCjE18zYAKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wNnUwNHfxcnSwBgIDUyhCvA5EawAjxsKckMjDDI9FQFTuNFd/'

    def _equalizar(self, json):
        r = [('qt_ganhador_faixa1', 'vr_rateio_faixa1'), ('qt_ganhador_faixa2', 'vr_rateio_faixa2'),
             ('qt_ganhador_faixa3', 'vr_rateio_faixa3')]

        if json != {}:
            equal = {}
            equal['alert'] = json[0]['error']

            if not equal['alert']:
                equal['premiacao'] = ['5 acertos', '4 acertos', '3 acertos']
                equal['nconcurso'] = json[0]['co_concurso']
                equal['datasort'] = json[0]['dt_apuracaoStr']
                equal['sorteadas'] = [[('+' if int(jogo['qt_gol_time1']) > 3 else jogo['qt_gol_time1']) + (
                    '+' if int(jogo['qt_gol_time2']) > 3 else jogo['qt_gol_time2']) for jogo in json]]
                equal['rateios'] = [[(str(json[0][tup[0]] or 0), locale.currency(json[0][tup[1]] or 0, grouping=True)) for tup in r]]
                equal['acumulado'] = locale.currency(json[0]['vr_ACUMULADO_FAIXA1'] or 0, grouping=True)
                equal['proximo'] = json[0]['dt_proximo_concursoStr']

            return equal
        return json


class Loteca(Sorteio):
    token = 'Z7_HGK818G0KOCO10AFFGUTGU0004'
    verificador = '04_Sj9CPykssy0xPLMnMz0vMAfGjzOLNDH0MPAzcDbz8vTxNDRy9_Y2NQ13CDA3cDYEKIoEKnN0dPUzMfQwMDEwsjAw8XZw8XMwtfQ0MPM2I02-AAzgaENIfrh-FqsQ9wBmoxN_FydLAGAgNTKEK8DkRrACPGwpyQyMMMj0VAbNnwlU!/'

    def _equalizar(self, json):
        r = [('qtGanhadorFaixa1', 'vrRateioFaixa1'), ('qtGanhadorFaixa2', 'vrRateioFaixa2')]

        if json != {}:
            equal = {}
            equal['alert'] = json['error']

            if not equal['alert']:
                equal['premiacao'] = ['14 acertos', '13 acertos']
                equal['nconcurso'] = str(json['concurso'])
                equal['datasort'] = json['dtApuracaoStr']
                equal['sorteadas'] = [[str(n) for jg in [(jogo['colunaMeio'], jogo['colunaUm'], jogo['colunaDois']) for jogo in json['jogos']] for n, c in enumerate(jg) if c]]
                # quant vem como string
                equal['rateios'] = [[(json[tup[0]] or '0', locale.currency(json[tup[1]] or 0, grouping=True)) for tup in r]]
                equal['acumulado'] = locale.currency(json['vrConcursoAcumulado'] or 0, grouping=True)
                equal['proximo'] = json['dt_proximo_concursoStr']

            return equal
        return json


objs = [Megasena, Duplasena, Diadesorte , Lotofacil, Lotomania, Quina, Timemania, Lotogol, Loteca, Federal]

        



