﻿#!/usr/bin/python
# coding: utf-8

__version__ = '0.1.0'

__copyright__ = 'Copyright 2018, @brjomma <brjomma@gmail.com>'
__license__ = 'MIT <https://mit-license.org/>'



import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 


import unittest
from resultados import Avulso, Pacote
from simulacoes import Simulacao
from lobras import parametrizar as ptz


class Test_Megasena_ResultadoAvulso(unittest.TestCase):
    """Os dados são exclusivamente argumentos de linha de comando"""

    def setUp(self):
        self.parser, self.modals = ptz()
        self.cont = 0
    
    def prepAvulso(self, comandlin):
        dvargs = vars(self.parser.parse_args(comandlin))
        dvargs['modals'] = self.modals
        return Avulso(dvargs)
  
    def test_dezenas_sorteadas(self):
        # prompt >>>br r -c 2000 -i
        r = self.prepAvulso(['--megasena', 'result', '-c', '2000'])
        self.assertEqual('-'.join(r._sorteios[0].sorteadas[0]), '03-06-10-17-34-37')
            
    def test_padrao_nao_confere_concurso_vencido(self):
        # prompt >>>br r -c 200 -d 12-15-19-34-44-51
        r = self.prepAvulso(['--megasena', 'result', '-c', '200', '-d', '12-15-19-34-44-51'])
        self.assertEqual(' - concurso vencido (conferência não recomendada), -V confere', r._sorteios[0]._alert)
            
    def test_confere_concurso_vencido(self):
        # prompt >>>br r -c 200 -d 12-15-19-34-44-51 -V
        r = self.prepAvulso(['--megasena', 'result', '-c', '200', '-d', '12-15-19-34-44-51', '-V'])
        for sorteio in r._sorteios:
            for msg in sorteio._console._dados:
                if ' - Parabéns!' in msg:
                    self.assertIn(' - Parabéns! aposta premiada (sena)', msg)

    def test_valor_acumulado(self):
        # prompt >>>br r -c 2078 
        r = self.prepAvulso(['--megasena', 'result', '-c', '2078'])
        self.assertEqual(r._sorteios[0].acumulado, 'R$ 2.817.514,15')
            
    def test_detecta_concurso_invalido(self):
        # prompt >>>br r -c 2079 10000
        r = self.prepAvulso(['--megasena', 'result', '-c', '2079', '10000'])
        self.assertEqual(' - concurso inválido ou não sorteado, verifique!', r._sorteios[1]._alert)
            
    def test_detecta_erro_no_servidor(self):
        # prompt >>>br r -c 1 2079
        r = self.prepAvulso(['--megasena', 'result', '-c', '1', '2079'])
        self.assertEqual(' - erro interno no servidor (500), tente mais tarde', r._sorteios[0]._alert)
            
    def test_detecta_dezenas_invalidas(self):
        # prompt >>>br r -d 02--11-15-31-36-39-40a -c 2078
        r = self.prepAvulso(['--megasena', 'result', '-d=02--11-15-31-36-39-40a', '-c=2078'])
        for sorteio in r._sorteios:
            self.assertIn(' - dezena(s) inválida(s) (-40a), verifique! (1 a 60)', sorteio._apostas[0].dezenas._msgs)
            
    def test_detecta_dezenas_duplicadas(self):
        # prompt >>>br r -d 01-03-07-44-55-9-41-44-1 -c 2078
        r = self.prepAvulso(['--megasena', 'result', '-d', '01-03-07-44-55-9-41-44-1' '-c=2078'])
        for sorteio in r._sorteios:
            self.assertIn(' - dezena(s) duplicada(s) (01-44), verifique!', sorteio._apostas[0].dezenas._msgs)
            
    def test_detecta_limites_min_max(self):
        # prompt >>>br r -d 25-35-01-03-05-07-44-46-48-49-55-57-59-60-41-44-13 10-20-02-12-15 -c 2078
        r = self.prepAvulso(['--megasena', 'result', '-d', '25-35-01-03-05-07-44-46-48-49-55-57-59-60-41-44-13', '10-20-02-12-15', '-c=2078'])
        for sorteio in r._sorteios:
            self.assertIn(' - núm. de dezenas inválido (16), verifique! (min: 6, máx: 15)', sorteio._apostas[0].dezenas._msgs)
            self.assertIn(' - núm. de dezenas inválido (5), verifique! (min: 6, máx: 15)', sorteio._apostas[1].dezenas._msgs)
            
    def test_confronta_dezenas_prevalidadas(self):
        # prompt >>>br r -d 02--11-15-31-36-39-40a -c 2078
        r = self.prepAvulso(['--megasena', 'result', '-d=02--11-15-31-36-39-40a', '-c=2078'])
        for sorteio in r._sorteios:
            self.assertEqual('-'.join(sorteio._apostas[0].dezenas.numeros), '02-11-15-31-36-39')
        
    def test_mensagem_aposta_premiada_sena(self):
        # prompt >>>br r -d 13-16-26-35-37-39-40 -c 2077
        r = self.prepAvulso(['--megasena', 'result', '-d=13-16-26-35-37-39-40', '-c=2077'])
        for sorteio in r._sorteios:
            for msg in sorteio._console._dados:
                if ' - Parabéns! aposta premiada (sena)' in msg:
                    self.cont += 1
        self.assertEqual(self.cont, 1)

    def test_mensagem_aposta_premiada_quina(self):
        # prompt >>>br r -d 02-11-15-31-36-39 -c 2078
        r = self.prepAvulso(['--megasena', 'result', '-d=02-11-15-31-36-39', '-c=2078'])
        for sorteio in r._sorteios:
            for msg in sorteio._console._dados:
                if ' - Parabéns! aposta premiada (quina)' in msg:
                    self.cont += 1
        self.assertEqual(self.cont, 1)

    def test_mensagem_aposta_premiada_quadra(self):
        # prompt >>>br r -d 05-11-15-31-36-39 -c 2078
        r = self.prepAvulso(['--megasena', 'result', '-d=05-11-15-31-36-39', '-c=2078'])
        for sorteio in r._sorteios:
            for msg in sorteio._console._dados:
                if ' - Parabéns! aposta premiada (quadra)' in msg:
                    self.cont += 1
        self.assertEqual(self.cont, 1)
                

    def test_valor_do_premio_sena(self):
        # prompt >>>br r -d 13-16-26-35-37-39-40 -c 2077
        r = self.prepAvulso(['--megasena', 'result', '-d=13-16-26-35-37-39-40', '-c=2077'])
        senarat=r._sorteios[0].premiadas[0][4]
        self.assertEqual(senarat, 'R$ 27.758.694,68')
        
    def test_valor_do_premio_quina(self):
        # prompt >>>br r -d 02-11-15-31-36-39 -c 2078
        r = self.prepAvulso(['--megasena', 'result', '-d=02-11-15-31-36-39', '-c=2078'])
        quinarat=r._sorteios[0].premiadas[0][4]
        self.assertEqual(quinarat, 'R$ 58.827,22')
        
    def test_valor_do_premio_quadra(self):
        # prompt >>>br r -d 05-11-15-31-36-39 -c 2078
        r = self.prepAvulso(['--megasena', 'result', '-d=05-11-15-31-36-39', '-c=2078'])
        quinarat=r._sorteios[0].premiadas[0][4]
        self.assertEqual(quinarat, 'R$ 804,79')
        
    def test_quantidade_de_concursos(self):
        # prompt >>>br r -d 02-11-15-31-36-39-40 -c 2077-2075 2078
        r = self.prepAvulso(['--megasena', 'result', '-d', '02-11-15-31-36-39-40', '-c', '2077-2075', '2078'])
        self.assertEqual(len(r._sorteios), 4)
        
    def test_quantidade_de_apostas(self):
        # prompt >>>br r -d 02-11-15-31-36-39-40 05-06-09-25-31-35-42 -c 2077-2075 2078
        r = self.prepAvulso(['--megasena', 'result', '-d', '02-11-15-31-36-39-40', '-c', '2077-2075', '2078'])
        for sorteio in r._sorteios:
            self.cont += len(sorteio._apostas)
        self.assertEqual(self.cont, 4)

    def test_quantidade_de_apostas2(self):
        # prompt >>>br r -c 2078 2060-2062 -d 02-11-15-31--36-39-40a 10-20-02-12-15-30-33-39 25-35-01-03-05-07-44-46-48-49-55-57-59-60-41-44-13
        r = self.prepAvulso(['--megasena', 'result', '-d', '02-11-15-31--36-39-40a', '10-20-02-12-15-30-33-39', '25-35-01-03-05-07-44-46-48-49-55-57-59-60-41-44-13', '-c', '2077-2075', '2078'])
        for sorteio in r._sorteios:
            self.cont += len(sorteio._apostas)
        self.assertEqual(self.cont, 12)

    def test_quantidade_de_apostas_premiadas(self):
        # prompt >>>br r -c 2078 2060-2062 -d 02-11-15-31--36-39-40a 10-20-02-12-15-30-33-39 25-35-01-03-05-07-44-46-48-49-55-57-59-60-41-44-13
        r = self.prepAvulso(['--megasena', 'result', '-d', '02-11-15-31--36-39-40a', '10-20-02-12-15-30-33-39', '25-35-01-03-05-07-44-46-48-49-55-57-59-60-41-44-13', '-c', '2077-2075', '2078'])
        for sorteio in r._sorteios:
            self.cont += len(sorteio.premiadas)
        self.assertEqual(self.cont, 2)


class Test_Megasena_ResultadoPacote(unittest.TestCase):
    """Utiliza os dados do arquivo ``test_dados.csv``"""
    
    def setUp(self):
        self.parser, self.modals = ptz()
        self.cont = 0
    
    def prepPacote(self, comandlin):
        dvargs = vars(self.parser.parse_args(comandlin))
        dvargs['modals'] = self.modals
        return Pacote(dvargs)
  
    def test_quantidade_de_alguns_concursos(self):
        # prompt >>>br r -f test_dados.csv -c 2031 2033
        p = self.prepPacote(['--megasena', 'result', '-f', 'test_dados.csv', '-c', '2031', '2033'])
        self.assertEqual(len(p._sorteios), 2)

    def test_quantidade_de_algumas_apostas(self):
        # prompt >>>br r -f test_dados.csv -c 2031 2033
        p = self.prepPacote(['--megasena', 'result', '-f', 'test_dados.csv', '-c', '2031', '2033'])
        for sorteio in p._sorteios:
            self.cont += len(sorteio._apostas)
        self.assertEqual(self.cont, 3)
        
    def test_quantidade_de_todos_os_concursos_do_pacote(self):
        # prompt >>>br r -f test_dados.csv
        p = self.prepPacote(['--megasena', 'result', '-f', 'test_dados.csv'])
        self.assertEqual(len(p._sorteios), 54)

    def test_quantidade_de_todas_as_apostas_do_pacote(self):
        # prompt >>>br r -f test_dados.csv
        p = self.prepPacote(['--megasena', 'result', '-f', 'test_dados.csv'])
        for sorteio in p._sorteios:
            self.cont += len(sorteio._apostas)
        self.assertEqual(self.cont, 143)

    def test_quantidade_de_apostas_premiadas(self):
        # prompt >>>br r -f test_dados.csv
        p = self.prepPacote(['--megasena', 'result', '-f', 'test_dados.csv'])
        for sorteio in p._sorteios:
            self.cont += len(sorteio.premiadas)
        self.assertEqual(self.cont, 0)

        
class Test_Megasena_Simulacao(unittest.TestCase):
    def setUp(self):
        self.parser, self.modals = ptz()
        self.cont = 0
    
    def prepSimulacao(self, comandlin):
        dvargs = vars(self.parser.parse_args(comandlin))
        dvargs['modals'] = self.modals
        return Simulacao(dvargs)
  
    def test_assegurar_que_a_quantidade_de_apostas_seja_igual_a_opcao_q(self):
        # prompt >>>br --mega s -q 1000
        s = self.prepSimulacao(['--megasena', 'simula', '-q=1000'])
        self.assertEqual(len(s._apostas), s._dvargs['q'])
        
    def test_assegurar_a_quantidade_de_apostas_com_simulacoes_variadas(self):
        # prompt >>>br --mega s -q 20 -d 10-24-30 15-25
        s = self.prepSimulacao(['--megasena', 'simula', '-q=20', '-d', '10-24-30', '15-25'])
        self.assertEqual(len(s._apostas), s._dvargs['q'] * len(s._dvargs['lst_dezenas']))
        
    def test_assegurar_que_o_tamanho_da_aposta_seja_igual_a_diretiva_n(self):
        # prompt >>>br --mega s -q 3 -n 7 10 -d 10-24-30
        s = self.prepSimulacao(['--megasena', 'simula', '-q=3', '-n', '7', '10', '-d=10-24-30'])
        for aposta in s._apostas:
            self.assertIn(len(aposta.dezenas.numeros), s._dvargs['lst_ndez'])

    def test_assegurar_que_as_dezenas_do_coracao_estejam_na_simulacao(self):
        # prompt >>>br --mega s -q 3 -n 10 -d 10-24-30a
        s = self.prepSimulacao(['--megasena', 'simula', '-q=3', '-n=10', '-d=10-24-30a'])
        for aposta in s._apostas:
            for dz in aposta.dezenas._lst_prevalidas:
                self.assertIn(dz, aposta.dezenas.numeros)
                
    def test_assegurar_que_as_dezenas_estejam_dentro_do_display(self):
        # prompt >>>br --mega s -q 100
        s = self.prepSimulacao(['--megasena', 'simula', '-q=100'])
        for aposta in s._apostas:
            for dz in aposta.dezenas.numeros:
                self.assertIn(int(dz), range(1, aposta.dezenas.display+1))
    

        
if __name__ == '__main__':
    unittest.main()

