﻿#!/usr/bin/python
# coding: utf-8

__version__ = '0.1.0'

__copyright__ = 'Copyright 2018, @brjomma <brjomma@gmail.com>'
__license__ = 'MIT <https://mit-license.org/>'


import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 


import unittest
from resumo import Resumo
from lobras import parametrizar as ptz

        
class Test_Resumo(unittest.TestCase):
    """Utiliza os dados do arquivo ``test_dados.csv``"""
    
    def setUp(self):
        self.parser, self.modals = ptz()
        self.cont = 0
    
    def prepResumo(self, comandlin):
        dvargs = vars(self.parser.parse_args(comandlin))
        dvargs['modals'] = self.modals
        return Resumo(dvargs)
  
    def test_assegurar_todas_as_modalidades(self):
        # prompt >>>br R
        R = self.prepResumo(['Resumo'])
        self.assertEqual(list(R._ultimos.keys()), R._dvargs['modals'])
        
    def test_assegurar_listagem_de_apostas_pendentes(self):
        # prompt >>>br R -f test_dados.csv
        R = self.prepResumo(['Resumo', '-f', 'test_dados.csv'])
        R._montatela()
        R._monta_pendentes()
        for linha in R._console._dados:
            if 'Concursos e apostas pendentes' in linha:
                self.cont += 1
        self.assertEqual(self.cont, 1)
        
    def test_assegurar_listagem_dos_resultados(self):
        # prompt >>>br R -r
        R = self.prepResumo(['Resumo', '-r'])
        self.assertEqual(len(R._sorteios), 10)

        
if __name__ == '__main__':
    unittest.main()

