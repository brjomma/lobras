﻿#!/usr/bin/python
# coding: utf-8

__version__ = '0.1.0'

__copyright__ = 'Copyright 2018, @brjomma <brjomma@gmail.com>'
__license__ = 'MIT <https://mit-license.org/>'


import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 


import unittest
from utils import sequenciar, dia_abrev, lerCSV

class Utils(unittest.TestCase):
        
    def test_sequenciar(self):
        res = sequenciar(123)
        self.assertEqual(res, ['123'])
        
        res = sequenciar(123-125)
        self.assertEqual(res, ['2'])
        
        res = sequenciar(['2069a2071'])
        self.assertEqual(res, ['2069', '2070', '2071'])
        
        lista = ['2069a2071', '2068', '2079-2075']
        res = sequenciar(lista)
        self.assertEqual(res, ['2068', '2069', '2070', '2071', '2075', '2076', '2077', '2078', '2079'])

        lista = ['2069a2071', '2069', '2079-2075']
        res = sequenciar(lista)
        self.assertEqual(res, ['2069', '2069', '2070', '2071', '2075', '2076', '2077', '2078', '2079'])
        
        res = sequenciar(['2069-2071'])
        self.assertEqual(res, ['2069', '2070', '2071'])
    
        res = sequenciar(['2069a2061'])
        self.assertEqual(res, ['2061', '2062', '2063', '2064', '2065', '2066', '2067', '2068', '2069'])
    
        res = sequenciar()
        self.assertEqual(res, [None])

        res = sequenciar(None)
        self.assertEqual(res, [None])
    
        res = sequenciar('--')
        self.assertEqual(res, [None])
        
    def test_dia_abrev(self):
            self.assertEqual(dia_abrev('21-05-2013'), 'Ter')
            self.assertEqual(dia_abrev('21/05-2013'), 'Erro')
            self.assertEqual(dia_abrev('21-15-2013'), 'Erro')
            self.assertEqual(dia_abrev('01/01/1970'), 'Qui')
            self.assertEqual(dia_abrev('31/12/9999'), 'Sex')
            self.assertEqual(dia_abrev('15,05,2019'), 'Qua')

    def test_lerCSV(self):
        """Leitura de dados em pacotes."""
        
        # Um nome errado
        filenames = ['minhasapostas.csv', 'test_dados.csv']
        dados, erros = lerCSV(filenames)
        self.assertIn('minhasapostas.csv', erros)
        self.assertNotEqual(dados, [])
        
        # sem informação de arquivo
        filenames = []
        dados, erros = lerCSV(filenames)
        self.assertEqual((dados, erros), ([], []))
    
        # arquivo vazio
        filenames = ['test_arquivo_vazio.csv']
        dados, erros = lerCSV(filenames)
        self.assertEqual((dados, erros), ([], []))
        
if __name__ == '__main__':
    unittest.main()
        