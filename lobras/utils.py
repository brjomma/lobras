﻿#!/usr/bin/python
# coding: utf-8

__version__ = '0.1.0'

__copyright__ = 'Copyright 2018, @brjomma <brjomma@gmail.com>'
__license__ = 'MIT <https://mit-license.org/>'


import os
import sys
import csv
from datetime import date
import time
from colorama import init, Fore, Back, Style
init()
import constantes as ct


def lerCSV(filenames=None):
    dados = []
    erros = []
    filenames = filenames or []

    for filename in filenames:
        try:
            with open(os.path.join(ct.MEUSDADOS, filename), newline='') as fcsv:
                reader = csv.reader(fcsv,  delimiter=',')
                # previa = [lin for lin in reader]
                dados.extend([[cel for cel in lin if cel] for lin in [read for read in reader]])

        except IOError:
            erros.append(filename)
        
    return dados, erros
    
def sep(flat):
    try:
        sep=[i for i in flat if not i.isdigit()][0]
    except:
        sep = None
    return sep
    
def verde(texto):
    return Fore.LIGHTGREEN_EX + texto + Fore.RESET 

def azul(texto):
    return Fore.LIGHTBLUE_EX + texto + Fore.RESET

def sequenciar(arg=None):
    # if not isinstance(arg, list) and not isinstance(arg, str) and not isinstance(arg, int) or arg is None:
    if not isinstance(arg, (list, str, int)) or arg is None:
        arg = [None]
    elif isinstance(arg, str) :
        arg = [arg]
    elif isinstance(arg, int) :
        arg = [str(arg)]
        
    if arg == [None]:
        return arg

    nconcs = []
    for nconc in arg:
        try:
            sep=[i for i in nconc if not i.isdigit()][0]
        except IndexError:
            sep=None
        fatias = nconc.split(sep=sep)
        
        # retira o que não é dígito
        #
        fatias = [fat for fat in fatias if fat.isdigit()]
        
        if not len(fatias):
            fatias = [None]
        if len(fatias) == 1:
            nconcs.append(*fatias)
        else:
            fatias.sort(key=int)
            ini = int(fatias[0])
            fim = int(fatias[1])
            for r in range(ini, fim+1):
                nconcs.append(str(r))
                
    return sorted(nconcs)

def dia_abrev(d):
    separad = sep(d)
    dds = ('Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom')
    try:
        dt = date(*[int(x) for x in reversed(d.split(sep=separad))])
    except:
        return 'Erro'
    return  dds[dt.weekday()]

def progress_bar(total, progresso, str):
    """
    Exibe uma barra de progressão na tela.

    Livre adaptação do original: https://stackoverflow.com/a/15860757/1391441
    """
    largura = 20
    progresso = float(progresso) / float(total)
    if progresso >= 1.:
        progresso = 1
    degrau = int(round(largura * progresso))
    bloco = "\r"+str+" {:3.0f}% {}".format(round(progresso * 100, 0),
                                                "\N{BLACK CIRCLE}" * degrau + "." * (largura - degrau))
    sys.stdout.write(bloco)
    sys.stdout.flush()

sobre = dict(
    SOBRE_MEGASENA='''Para realizar o jogo você deve marcar de 6 a 15 números dentre os 60 disponíveis no volante de apostas. Ganha o acertador dos 6 números sorteados, ou 4 ou 5 números. Os sorteios são realizados duas vezes por semana, às quartas e aos sábados. 
    '''
    ,
    SOBRE_DUPLASENA='''Para jogar, escolha de 6 a 15 números dentre os 50 disponíveis. São dois sorteios por concurso e ganha acertando 3, 4, 5 ou 6 números no primeiro e/ou segundo sorteios. É sorteada às terças, quintas e sábados.
    '''
    ,
    SOBRE_LOTOFACIL='''Marque entre 15 a 18 números, dentre os 25 disponíveis no volante. Fatura o prêmio quem acertar 11, 12, 13, 14 ou 15 números. Os sorteios são realizados às segundas, quartas e sextas.
    '''
    ,
    SOBRE_LOTOMANIA='''Escolha 50 números e concorra a prêmios para acertos de 20, 19, 18, 17, 16, 15 ou nenhum número. Os sorteios são realizados às terças e às sextas.
    '''
    ,
    SOBRE_QUINA='''Marque de 5 a 15 números dentre os 80 disponíveis no volante. Ganham prêmios os acertadores de 2, 3, 4 ou 5 números. São 6 sorteios semanais, de segunda a sábado.
    '''
    ,
    SOBRE_TIMEMANIA='''Para jogar escolha 10 números entre os 80 disponíveis e um "Time do Coração". São sorteados 7 números e um Time do Coração por concurso. Se você tiver de 3 a 7 acertos, ou acertar o time do coração, ganha. Os sorteios são realizados nas terças, quintas e sábados.
    '''
    ,
    SOBRE_LOTOGOL='''Para apostar, basta marcar no volante o número de gols de cada time de futebol participante dos 5 jogos do concurso. Você pode assinalar 0, 1, 2, 3 ou mais gols (opção está representada pelo sinal +). Os clubes participantes estão impressos nos bilhetes emitidos pelo terminal. Ganha acertando 5, 4 ou 3 resultados. Os concursos são realizados semanalmente.
    '''
    ,
    SOBRE_LOTECA='''Para apostar, basta marcar o seu palpite para cada um dos 14 jogos do concurso assinalando uma das três colunas, duas delas (duplo) ou três (triplo). Os clubes participantes estão impressos nos bilhetes emitidos pelo terminal. Ganha quem acertar 14 ou 13 jogos. A aposta mínima dá direito a um duplo.
    '''
    ,
    SOBRE_DIADESORTE='''Escolha de 7 a 15 números dentre os 31 disponíveis e mais 1 "Mês de Sorte". São sorteados 7 números e um “Mês de Sorte” por concurso. Os sorteios são realizados nas terças, quintas e sábados.
    '''
    ,
    SOBRE_FEDERAL='''Compre um bilhete (inteiro ou fração). Você ganha acertando um dos cinco prêmios principais: Milhar, centena ou dezena de qualquer um dos números sorteados nos cinco prêmios principais. Bilhetes cujos números contenham a dezena final idêntica a umas das 3 (três) dezenas anteriores ou das 3 (três) dezenas posteriores à dezena do número sorteado para o 1º prêmio, excetuando-se os premiados pela aproximação anterior e posterior. A unidade do primeiro prêmio.'''      
)    

MS = {
    1:'JANEIRO', 
    2:'FEVEREIRO', 
    3:'MARÇO', 
    4:'ABRIL', 
    5:'MAIO', 
    6:'JUNHO', 
    7:'JULHO', 
    8:'AGOSTO', 
    9:'SETEMBRO', 
    10:'OUTUBRO', 
    11:'NOVEMBRO', 
    12:'DEZEMBRO'
    }

TC={
    1:'ABC/RN',
    2:'AMÉRICA/MG',
    3:'AMÉRICA/RJ',
    4:'AMÉRICA/RN',
    5:'AMERICANO/RJ',
    6:'ATLÉTICO/GO',
    7:'ATLÉTICO/MG',
    8:'ATLÉTICO/PR',
    9:'AVAÍ/SC',
    10:'BAHIA/BA',
    11:'BANGU/RJ',
    12:'BARUERI/SP',
    13:'BOTAFOGO/PB',
    14:'BOTAFOGO/RJ',
    15:'BRAGANTINO/SP',
    16:'BRASILIENSE/DF',
    17:'CEARÁ/CE',
    18:'CORINTHIANS/SP',
    19:'CORITIBA/PR',
    20:'CRB/AL',
    21:'CRICIÚMA/SC',
    22:'CRUZEIRO/MG',
    23:'CSA/AL',
    24:'DESPORTIVA/ES',
    25:'FIGUEIRENSE/SC',
    26:'FLAMENGO/RJ',
    27:'FLUMINENSE/RJ',
    28:'FORTALEZA/CE',
    29:'GAMA/DF',
    30:'GOIÁS/GO',
    31:'GRÊMIO/RS',
    32:'GUARANI/SP',
    33:'INTER DE LIMEIRA/SP',
    34:'INTERNACIONAL/RS',
    35:'IPATINGA/MG',
    36:'ITUANO/SP',
    37:'JI-PARANÁ/RO',
    38:'JOINVILLE/SC',
    39:'JUVENTUDE/RS',
    40:'JUVENTUS/SP',
    41:'LONDRINA/PR',
    42:'MARÍLIA/SP',
    43:'MIXTO/MT',
    44:'MOTO CLUBE/MA',
    45:'NACIONAL/AM',
    46:'NÁUTICO/PE',
    47:'OLARIA/RJ',
    48:'OPERÁRIO/MS',
    49:'PALMAS/TO',
    50:'PALMEIRAS/SP',
    51:'PARANÁ/PR',
    52:'PAULISTA/SP',
    53:'PAYSANDÚ/PA',
    54:'PONTE PRETA/SP',
    55:'PORT. DE DESPORTOS/SP',
    56:'REMO/PA',
    57:'RIO BRANCO/AC',
    58:'RIO BRANCO/ES',
    59:'RIVER/PI',
    60:'RORAIMA/RR',
    61:'SAMPAIO CORRÊA/MA',
    62:'SANTA CRUZ/PE',
    63:'SANTO ANDRÉ/SP',
    64:'SANTOS/SP',
    65:'SÃO CAETANO/SP',
    66:'SÃO PAULO/SP',
    67:'SÃO RAIMUNDO/AM',
    68:'SERGIPE/SE',
    69:'SPORT/PE',
    70:'TREZE/PB',
    71:'TUNA LUSO/PA',
    72:'UBERLÂNDIA/MG',
    73:'UNIÃO  BARBARENSE/SP',
    74:'UNIÃO SÃO JOÃO/SP',
    75:'VASCO DA GAMA/RJ',
    76:'VILA NOVA/GO',
    77:'VILLA NOVA/MG',
    78:'VITÓRIA/BA',
    79:'XV DE PIRACICABA/SP',
    80:'YPIRANGA/AP'
}